Salaxy WinForms Demo (.Net Framework)
=====================================

This is a demo project for using Salaxy API from .Net Framework and especially from Windows Forms.

Prerequisites
-------------

The document assumes that you have a basic idea of Palkkaus.fi and Salaxy API and that you are experienced in modern .Net development. You should have Visual Studio 2019 with latest updates or later version installed.

Also, we assume you already have a developer account (UI/PWD) to our test environment. You may create this by following the instructions in https://developers.salaxy.com/#/security/index

Demo project
------------

A .Net Windows Forms based demo project is available in https://gitlab.com/salaxy/winforms-demo it is open without any authentication. Though the example is made for WinForms, it should give a good idea on how to use API’s in other .Net environments Asp.Net forms, Asp.Net MVC, WPF and .Net 6 (.Net Core).

To get started:

- Clone the project from: https://gitlab.com/salaxy/winforms-demo.git
- Build in Visual Studio .Net
- NOTE: We are using WebView2 (https://docs.microsoft.com/en-us/microsoft-edge/webview2/) to demonstrate embedded Web UI’s and in the current version there seems to be some issues with that 
  - TODO: Instructions on how to install / how to assure WebView2 is working.

Contents / How to
-----------------

- [Generating API proxy classes](./docs/proxy-classes.md)
- [Payroll import process](./docs/payroll-import.md) and creating a new Payroll
