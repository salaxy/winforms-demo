﻿
namespace SalaxyDemo.Forms
{
    partial class FrmCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCalculator));
            this.calculatorControl = new SalaxyDemo.Forms.CalculatorControl();
            this.SuspendLayout();
            // 
            // calculatorControl
            // 
            this.calculatorControl.Api = null;
            this.calculatorControl.Calc = null;
            this.calculatorControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calculatorControl.Location = new System.Drawing.Point(0, 0);
            this.calculatorControl.Name = "calculatorControl";
            this.calculatorControl.Size = new System.Drawing.Size(800, 450);
            this.calculatorControl.TabIndex = 0;
            // 
            // FrmCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.calculatorControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCalculator";
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.FrmCalculator_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CalculatorControl calculatorControl;
    }
}