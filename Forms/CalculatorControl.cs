﻿using Salaxy;
using SalaxyDemo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    /// <summary>
    /// Simple demo user interface for calculator.
    /// </summary>
    public partial class CalculatorControl : UserControl
    {
        public CalculatorControl()
        {
            InitializeComponent();
            gridRows.AutoGenerateColumns = false;
        }

        /// <summary>
        /// Calculation that is shown in the control UI.
        /// </summary>
        public Salaxy.Calculation Calc
        {
            get
            {
                return calculationBindingSource.DataSource as Calculation;
            }
            set
            {
                calculationBindingSource.DataSource = value;
                if (value?.Rows != null)
                {
                    initGrid();
                    gridRows.DataSource = value.Rows;
                }
            }
        }

        private void initGrid()
        {
            if (gridRows.Columns.Count > 0)
            {
                return;
            }
            gridRows.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "RowIndex",
                HeaderText = "Ix",
                DataPropertyName = "RowIndex",
                Width = 50,
                ReadOnly = true
            });
            gridRows.Columns.Add(new DataGridViewComboBoxColumn()
            {
                Name = "RowType",
                HeaderText = "Type",
                DataPropertyName = "RowType",
                DataSource = Enum.GetValues(typeof(CalculationRowType)),
                ValueType = typeof(CalculationRowType)
            });
            // 
            gridRows.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Count",
                HeaderText = "Count",
                DataPropertyName = "Count",
            });
            gridRows.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Price",
                HeaderText = "Price",
                DataPropertyName = "Price",
            });
            gridRows.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "Message",
                HeaderText = "Message",
                DataPropertyName = "Message",
            });
            gridRows.Columns.Add(new DataGridViewComboBoxColumn()
            {
                Name = "Unit",
                HeaderText = "Unit",
                DataPropertyName = "Unit",
                DataSource = Enum.GetValues(typeof(CalculationRowUnit)),
                ValueType = typeof(CalculationRowUnit)
            });
            gridRows.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "SourceId",
                HeaderText = "SourceId",
                DataPropertyName = "SourceId",
            });
        }

        /// <summary>
        /// Api helper that is used
        /// </summary>
        public SalaxyApiHelper Api { get; set; }

        private void requestedSalaryDateLabel_Click(object sender, EventArgs e)
        {

        }

        private void requestedSalaryDateTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private async void btnRecalculate_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Calc.Info.CostCenter);
            Calc = await (Api ?? new SalaxyApiHelper()).Recalculate(Calc);
            tabsControl.SelectedTab = tabResults;
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            if (Api == null || !Api.IsAuthenticated)
            {
                // TODO: handle situation when window is opened first and authentication is done after that.
                MessageBox.Show("Not authenticated: cannot save.");
                return;
            }
            Calc = await (Api ?? new SalaxyApiHelper()).SaveItem(ApiListItemType.Calculation, Calc);
            tabsControl.SelectedTab = tabResults;
        }

        private void btnAddRow_Click(object sender, EventArgs e)
        {
            Calc.Rows.Add(new UserDefinedRow() {
                Data = new Dictionary<string, object>(),
                RowType = CalculationRowType.Salary,
            });
            // TODO: There is probalby a better way of doing this, but this is just a demo.
            gridRows.DataSource = null;
            gridRows.DataSource = Calc.Rows; 
        }

        private void deductionTaxAndSocialSecuritySelfPaymentLabel_Click(object sender, EventArgs e)
        {

        }

        private void calculationBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
