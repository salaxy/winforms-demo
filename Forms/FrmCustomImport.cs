﻿using SalaxyDemo.Model;
using SalaxyDemo.Model.ImportDemo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    public partial class FrmCustomImport : Form
    {
        /// <summary>
        /// Helper for connecting to Salaxy API.
        /// </summary>
        public SalaxyApiHelper Api { get; set; }

        /// <summary>
        /// Service that actually impements everything: Look at this code for how the import works.
        /// </summary>
        public SomeCustomImportService Service { get; set; }

        public FrmCustomImport()
        {
            InitializeComponent();
            Service = new SomeCustomImportService();
            initGrid();
        }

        /// <summary>
        /// Gets or sets the items that are bound 
        /// </summary>
        public List<SomeCustomImportRow> Items
        {
            get 
            {
                return dataGrid.DataSource as List<SomeCustomImportRow> ?? new List<SomeCustomImportRow>();
            }
            set
            {
                dataGrid.DataSource = value ?? new List<SomeCustomImportRow>();
            }
        }

        private void initGrid()
        {
            if (dataGrid.Columns.Count > 0)
            {
                return;
            }
            dataGrid.AutoGenerateColumns = false;
            dataGrid.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "PersonalId",
                HeaderText = "Worker ID",
                DataPropertyName = "PersonalId",
            });
            dataGrid.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "HoursOrCount",
                HeaderText = "Hours",
                DataPropertyName = "HoursOrCount",
            });
            dataGrid.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = "PricePerHour",
                HeaderText = "Price per hour",
                DataPropertyName = "PricePerHour",
            });
            dataGrid.Columns.Add(new DataGridViewComboBoxColumn()
            {
                Name = "Type",
                HeaderText = "Type",
                DataPropertyName = "Type",
                DataSource = Enum.GetValues(typeof(SomeCustomImportRowType)),
                ValueType = typeof(SomeCustomImportRowType)
            });
            dataGrid.Columns.Add(new DataGridViewComboBoxColumn()
            {
                Name = "SalaxyType",
                HeaderText = "SalaxyType",
                DataPropertyName = "SalaxyType",
                DataSource = Enum.GetValues(typeof(Salaxy.CalculationRowType)),
                ValueType = typeof(Salaxy.CalculationRowType)
            });
        }

        private void btnLoadDemoData_Click(object sender, EventArgs e)
        {
            Items = Service.GetDemoRows();
        }

        private void btnDoImport_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
