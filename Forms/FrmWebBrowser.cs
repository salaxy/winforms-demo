﻿using SalaxyDemo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    /// <summary>
    /// Shows embedded web UI in a browser view
    /// </summary>
    public partial class FrmWebBrowser : Form
    {
        string baseUrl;

        public FrmWebBrowser()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the URl fro 
        /// </summary>
        /// <param name="url">URL to set </param>
        /// <param name="api">
        /// API for authenticated call - for setting the token after the URL.
        /// If null or IsAuthenticated=false, the call is made anonymous.
        /// </param>
        public void SetUrl(string url, SalaxyApiHelper api = null)
        {
            webView.NavigationCompleted += WebView_NavigationCompleted;
            baseUrl = url;
            if (api != null && api.IsAuthenticated)
            {
                url = $"{url}#access_token={api.Token}";
            }
            webView.Source = new Uri(url);
        }



        private void WebView_NavigationCompleted(object sender, Microsoft.Web.WebView2.Core.CoreWebView2NavigationCompletedEventArgs e)
        {
            toolStripContainer1.Enabled = true;
        }

        /// <summary>
        /// Navigation button clicked. URL to navigate to is in the Tag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void naviButtonClicked(object sender, EventArgs e)
        {
            var tag = (sender as ToolStripButton)?.Tag as string;
            if (string.IsNullOrWhiteSpace(tag))
            {
                MessageBox.Show("No URL: The URL for navigation should be in the Tag of the button.");
            }
            else
            {
                if (tag.StartsWith("#"))
                {
                    tag = baseUrl + tag;
                }
                webView.CoreWebView2.Navigate(tag);
            }
        }
    }
}
