﻿using Salaxy;
using SalaxyDemo.Forms;
using SalaxyDemo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    /// <summary>
    /// The main parent form
    /// </summary>
    public partial class FrmMain : Form
    {
        public SalaxyApiHelper Api;

        public FrmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the application status text
        /// </summary>
        /// <param name="status"></param>
        public void SetStatus(string status)
        {
            toolStatus.Text = status;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Api = new SalaxyApiHelper();
            SetStatus("Demo app started (ANON).");
        }

        private async void recalculateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var api = new CalculatorClient(Api.GetAnonClient()) { BaseUrl = Api.Server };
            var model = await api.CreateNewAsync();
            model.Rows.Add(new UserDefinedRow()
            {
                Count = 10,
                Price = 15,
                RowType = CalculationRowType.HourlySalary,
            });
            model = await api.RecalculateAsync(model);
            var frm = new FrmCalculator();
            frm.SetCalculation(model, null);
            frm.MdiParent = this;
            frm.Text = "Anon calculator demo";
            frm.Show();
        }

        private async void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var login = new FrmLogin();
            if (login.ShowDialog(this) == DialogResult.OK)
            {
                SetStatus("Login started...");
                string env = ((KeyValuePair<string, string>)login.cbxEnvironments.SelectedItem).Value;
                if (login.cbxRemember.Checked)
                {
                    Properties.Settings.Default.UserName = (login.txtUid.Text ?? "").Trim();
                    Properties.Settings.Default.Password = (login.txtPwd.Text ?? "").Trim();
                    Properties.Settings.Default.Server = env;
                    Properties.Settings.Default.Save();
                }
                if (string.IsNullOrWhiteSpace(login.txtUid.Text) || string.IsNullOrWhiteSpace(login.txtPwd.Text))
                {
                    login.Dispose();
                    SetStatus("Login canceled: no uid/pwd.");
                }
                try
                {
                    var task = Api.DoPasswordAuthorization(login.txtUid.Text, login.txtPwd.Text, env);
                    login.Dispose();
                    await task;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error in login: " + ex.Message, "ERROR");
                    return;
                }
                // Add the inverse into logout
                setMenuEnabled(false);
                mnuFileNew.Enabled = true;
                statusSession.Text = $"User {Api.Session.Avatar.DisplayName} ({Api.Uid}) in {Api.Server}";
                SetStatus("Login complete ");
            }
            else
            {
                login.Dispose();
                SetStatus("Login canceled.");
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setMenuEnabled(true);
            mnuFileNew.Enabled = false;
            Api.LogOut();
            statusSession.Text = "Anonymous user";
            SetStatus("User logged out.");
        }

        private void setMenuEnabled(bool isLogOut)
        {
            loginToolStripMenuItem.Enabled = isLogOut;
            logoutToolStripMenuItem.Enabled = !isLogOut;
            listsToolStripMenuItem.Enabled = !isLogOut;
            demosToolStripMenuItem.Enabled = !isLogOut;
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void tileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void tileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private async void calculationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetStatus("Fetching calculations");
            await FrmGrid.OpenMdi(this, ApiListItemType.Calculation);
            SetStatus("Calculations fetch complete");
        }

        private async void paidCalculationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetStatus("Fetching paid calculations");
            await FrmGrid.OpenMdi(this, ApiListItemType.CalculationPaid);
            SetStatus("Paid calculations fetch complete");
        }

        private async void workersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetStatus("Fetching Workers");
            await FrmGrid.OpenMdi(this, ApiListItemType.WorkerAccount);
            SetStatus("Workers complete");
        }

        private async void employmentRelationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetStatus("Fetching Employment relations");
            await FrmGrid.OpenMdi(this, ApiListItemType.Employment);
            SetStatus("Employment relations complete");
        }

        private void btnFullUi_Click(object sender, EventArgs e)
        {
            var newForm = new FrmWebBrowser();
            newForm.MdiParent = this;
            newForm.SetUrl("https://demo-embed.palkkaus.fi/embedded.html?lang=en&skin=winforms", Api);
            newForm.Show();
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var newForm = new FrmCustomImport();
            newForm.MdiParent = this;
            newForm.Api = this.Api;
            newForm.Show();
        }

        private async void payrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetStatus("Fetching Employment relations");
            await FrmGrid.OpenMdi(this, ApiListItemType.PayrollDetails);
            SetStatus("Employment relations complete");
        }

        private async void mnuNewPayroll_Click(object sender, EventArgs e)
        {
            // Some defaults - these would of course depend on the case.
            var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            PayrollDetails newItem = new PayrollDetails() {
                Input = new PayrollInput
                {
                    NoUpdateFromEmployment = true, // Meaning: When adding calculations, do not take default rows from the employment relation.
                    Period = new DateRange()
                    {
                        Start = start,
                        End = start.AddMonths(1).AddDays(-1),
                        DaysCount = 15,
                    },
                    SalaryDate = start.AddMonths(1).AddDays(4),
                    WageBasis = WageBasis.Hourly,
                    Title = "Demo payroll",
                },
                Info = new Payroll03Info(),
            };
            var newForm = new FrmPayroll();
            newForm.SetPayroll(newItem, Api);
            newForm.MdiParent = this;
            newForm.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
