﻿using SalaxyDemo.Model;
using SalaxyDemo.Model.ImportDemo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    public partial class PayrollControl : UserControl
    {
        public PayrollControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Api helper that is used
        /// </summary>
        public SalaxyApiHelper Api { get; set; }

        /// <summary>
        /// Calculation that is shown in the control UI.
        /// </summary>
        public Salaxy.PayrollDetails Payroll
        {
            get
            {
                return payrollDetailsBindingSource.DataSource as Salaxy.PayrollDetails;
            }
            set
            {
                payrollDetailsBindingSource.DataSource = value;
                /*
                if (value?.Calcs != null)
                {
                    initGrid();
                    gridRows.DataSource = value.Calcs;
                }
                */
            }
        }

        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == tabDetails && Api != null && Api.IsAuthenticated && Payroll?.Id != null)
            {
                if (webView.Source == null)
                {
                    string url = $"https://demo-embed.palkkaus.fi/embedded.html?lang=en&skin=winforms#/payroll/details/{Payroll.Id}#access_token={Api.Token}";
                    webView.Source = new Uri(url);
                }
                else
                {
                    webView.Reload();
                }
            }
        }

        private async void btnImport_Click(object sender, EventArgs e)
        {
            var importForm = new FrmCustomImport();
            var result = importForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                var importSrv = new SomeCustomImportService();
                var importResult = await importSrv.ImportRows(Payroll, importForm.Items, Api);
                if (importResult != null)
                {
                    MessageBox.Show("Import ready!");
                }
            }
        }

        private void webView_Click(object sender, EventArgs e)
        {

        }

        private void payrollDetailsBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private async void btnSaveChanges_Click(object sender, EventArgs e)
        {
            Payroll = await Api.SaveItem(Salaxy.ApiListItemType.PayrollDetails, payrollDetailsBindingSource.DataSource);
        }
    }
}
