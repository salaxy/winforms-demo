﻿
namespace SalaxyDemo.Forms
{
    partial class PayrollControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayrollControl));
            System.Windows.Forms.Label totalPaymentLabel;
            System.Windows.Forms.Label totalGrossSalaryLabel;
            System.Windows.Forms.Label salaryDateLabel;
            System.Windows.Forms.Label paymentDateLabel;
            System.Windows.Forms.Label isReadyForPaymentLabel;
            System.Windows.Forms.Label feesLabel;
            System.Windows.Forms.Label dueDateLabel;
            System.Windows.Forms.Label dateLabel;
            System.Windows.Forms.Label calcCountLabel;
            System.Windows.Forms.Label titleLabel;
            System.Windows.Forms.Label noUpdateFromEmploymentLabel;
            System.Windows.Forms.Label startLabel;
            System.Windows.Forms.Label endLabel;
            System.Windows.Forms.Label daysCountLabel;
            System.Windows.Forms.Label salaryDateLabel2;
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnImport = new System.Windows.Forms.ToolStripButton();
            this.btnCheckPersonalIds = new System.Windows.Forms.ToolStripButton();
            this.tabDetails = new System.Windows.Forms.TabPage();
            this.webView = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.tabOverview = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.totalPaymentLabel1 = new System.Windows.Forms.Label();
            this.totalGrossSalaryLabel1 = new System.Windows.Forms.Label();
            this.salaryDateLabel1 = new System.Windows.Forms.Label();
            this.paymentDateLabel1 = new System.Windows.Forms.Label();
            this.isReadyForPaymentLabel1 = new System.Windows.Forms.Label();
            this.feesLabel1 = new System.Windows.Forms.Label();
            this.dueDateLabel1 = new System.Windows.Forms.Label();
            this.dateLabel1 = new System.Windows.Forms.Label();
            this.calcCountLabel1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.noUpdateFromEmploymentCheckBox = new System.Windows.Forms.CheckBox();
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.endDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.daysCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.salaryDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.payrollDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSaveChanges = new System.Windows.Forms.Button();
            totalPaymentLabel = new System.Windows.Forms.Label();
            totalGrossSalaryLabel = new System.Windows.Forms.Label();
            salaryDateLabel = new System.Windows.Forms.Label();
            paymentDateLabel = new System.Windows.Forms.Label();
            isReadyForPaymentLabel = new System.Windows.Forms.Label();
            feesLabel = new System.Windows.Forms.Label();
            dueDateLabel = new System.Windows.Forms.Label();
            dateLabel = new System.Windows.Forms.Label();
            calcCountLabel = new System.Windows.Forms.Label();
            titleLabel = new System.Windows.Forms.Label();
            noUpdateFromEmploymentLabel = new System.Windows.Forms.Label();
            startLabel = new System.Windows.Forms.Label();
            endLabel = new System.Windows.Forms.Label();
            daysCountLabel = new System.Windows.Forms.Label();
            salaryDateLabel2 = new System.Windows.Forms.Label();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.webView)).BeginInit();
            this.tabOverview.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daysCountNumericUpDown)).BeginInit();
            this.tabControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.payrollDetailsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tabControl);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(959, 554);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(959, 579);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnImport,
            this.btnCheckPersonalIds});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(172, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // btnImport
            // 
            this.btnImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnImport.Image = ((System.Drawing.Image)(resources.GetObject("btnImport.Image")));
            this.btnImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(47, 22);
            this.btnImport.Text = "&Import";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnCheckPersonalIds
            // 
            this.btnCheckPersonalIds.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnCheckPersonalIds.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckPersonalIds.Image")));
            this.btnCheckPersonalIds.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCheckPersonalIds.Name = "btnCheckPersonalIds";
            this.btnCheckPersonalIds.Size = new System.Drawing.Size(113, 22);
            this.btnCheckPersonalIds.Text = "Check &Personal Id\'s";
            // 
            // tabDetails
            // 
            this.tabDetails.Controls.Add(this.webView);
            this.tabDetails.Location = new System.Drawing.Point(4, 22);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetails.Size = new System.Drawing.Size(951, 528);
            this.tabDetails.TabIndex = 2;
            this.tabDetails.Text = "Details (full UI)";
            this.tabDetails.UseVisualStyleBackColor = true;
            // 
            // webView
            // 
            this.webView.AllowExternalDrop = true;
            this.webView.CreationProperties = null;
            this.webView.DefaultBackgroundColor = System.Drawing.Color.White;
            this.webView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webView.Location = new System.Drawing.Point(3, 3);
            this.webView.Name = "webView";
            this.webView.Size = new System.Drawing.Size(945, 522);
            this.webView.TabIndex = 0;
            this.webView.ZoomFactor = 1D;
            this.webView.Click += new System.EventHandler(this.webView_Click);
            // 
            // tabOverview
            // 
            this.tabOverview.Controls.Add(this.label1);
            this.tabOverview.Controls.Add(this.groupBox2);
            this.tabOverview.Controls.Add(this.groupBox1);
            this.tabOverview.Location = new System.Drawing.Point(4, 22);
            this.tabOverview.Name = "tabOverview";
            this.tabOverview.Padding = new System.Windows.Forms.Padding(3);
            this.tabOverview.Size = new System.Drawing.Size(951, 528);
            this.tabOverview.TabIndex = 0;
            this.tabOverview.Text = "Overview";
            this.tabOverview.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(calcCountLabel);
            this.groupBox1.Controls.Add(this.calcCountLabel1);
            this.groupBox1.Controls.Add(dateLabel);
            this.groupBox1.Controls.Add(this.dateLabel1);
            this.groupBox1.Controls.Add(dueDateLabel);
            this.groupBox1.Controls.Add(this.dueDateLabel1);
            this.groupBox1.Controls.Add(feesLabel);
            this.groupBox1.Controls.Add(this.feesLabel1);
            this.groupBox1.Controls.Add(isReadyForPaymentLabel);
            this.groupBox1.Controls.Add(this.isReadyForPaymentLabel1);
            this.groupBox1.Controls.Add(paymentDateLabel);
            this.groupBox1.Controls.Add(this.paymentDateLabel1);
            this.groupBox1.Controls.Add(salaryDateLabel);
            this.groupBox1.Controls.Add(this.salaryDateLabel1);
            this.groupBox1.Controls.Add(totalGrossSalaryLabel);
            this.groupBox1.Controls.Add(this.totalGrossSalaryLabel1);
            this.groupBox1.Controls.Add(totalPaymentLabel);
            this.groupBox1.Controls.Add(this.totalPaymentLabel1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(717, 149);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Info (read-only)";
            // 
            // totalPaymentLabel1
            // 
            this.totalPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.TotalPayment", true));
            this.totalPaymentLabel1.Location = new System.Drawing.Point(497, 46);
            this.totalPaymentLabel1.Name = "totalPaymentLabel1";
            this.totalPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalPaymentLabel1.TabIndex = 19;
            this.totalPaymentLabel1.Text = "label1";
            // 
            // totalPaymentLabel
            // 
            totalPaymentLabel.AutoSize = true;
            totalPaymentLabel.Location = new System.Drawing.Point(377, 46);
            totalPaymentLabel.Name = "totalPaymentLabel";
            totalPaymentLabel.Size = new System.Drawing.Size(78, 13);
            totalPaymentLabel.TabIndex = 18;
            totalPaymentLabel.Text = "Total Payment:";
            // 
            // totalGrossSalaryLabel1
            // 
            this.totalGrossSalaryLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.TotalGrossSalary", true));
            this.totalGrossSalaryLabel1.Location = new System.Drawing.Point(497, 70);
            this.totalGrossSalaryLabel1.Name = "totalGrossSalaryLabel1";
            this.totalGrossSalaryLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalGrossSalaryLabel1.TabIndex = 17;
            this.totalGrossSalaryLabel1.Text = "label1";
            // 
            // totalGrossSalaryLabel
            // 
            totalGrossSalaryLabel.AutoSize = true;
            totalGrossSalaryLabel.Location = new System.Drawing.Point(377, 70);
            totalGrossSalaryLabel.Name = "totalGrossSalaryLabel";
            totalGrossSalaryLabel.Size = new System.Drawing.Size(96, 13);
            totalGrossSalaryLabel.TabIndex = 16;
            totalGrossSalaryLabel.Text = "Total Gross Salary:";
            // 
            // salaryDateLabel1
            // 
            this.salaryDateLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.SalaryDate", true));
            this.salaryDateLabel1.Location = new System.Drawing.Point(126, 93);
            this.salaryDateLabel1.Name = "salaryDateLabel1";
            this.salaryDateLabel1.Size = new System.Drawing.Size(141, 23);
            this.salaryDateLabel1.TabIndex = 15;
            this.salaryDateLabel1.Text = "label1";
            // 
            // salaryDateLabel
            // 
            salaryDateLabel.AutoSize = true;
            salaryDateLabel.Location = new System.Drawing.Point(6, 93);
            salaryDateLabel.Name = "salaryDateLabel";
            salaryDateLabel.Size = new System.Drawing.Size(65, 13);
            salaryDateLabel.TabIndex = 14;
            salaryDateLabel.Text = "Salary Date:";
            // 
            // paymentDateLabel1
            // 
            this.paymentDateLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.PaymentDate", true));
            this.paymentDateLabel1.Location = new System.Drawing.Point(126, 70);
            this.paymentDateLabel1.Name = "paymentDateLabel1";
            this.paymentDateLabel1.Size = new System.Drawing.Size(141, 23);
            this.paymentDateLabel1.TabIndex = 11;
            this.paymentDateLabel1.Text = "label1";
            // 
            // paymentDateLabel
            // 
            paymentDateLabel.AutoSize = true;
            paymentDateLabel.Location = new System.Drawing.Point(6, 70);
            paymentDateLabel.Name = "paymentDateLabel";
            paymentDateLabel.Size = new System.Drawing.Size(77, 13);
            paymentDateLabel.TabIndex = 10;
            paymentDateLabel.Text = "Payment Date:";
            // 
            // isReadyForPaymentLabel1
            // 
            this.isReadyForPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.IsReadyForPayment", true));
            this.isReadyForPaymentLabel1.Location = new System.Drawing.Point(497, 116);
            this.isReadyForPaymentLabel1.Name = "isReadyForPaymentLabel1";
            this.isReadyForPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.isReadyForPaymentLabel1.TabIndex = 9;
            this.isReadyForPaymentLabel1.Text = "label1";
            // 
            // isReadyForPaymentLabel
            // 
            isReadyForPaymentLabel.AutoSize = true;
            isReadyForPaymentLabel.Location = new System.Drawing.Point(377, 116);
            isReadyForPaymentLabel.Name = "isReadyForPaymentLabel";
            isReadyForPaymentLabel.Size = new System.Drawing.Size(114, 13);
            isReadyForPaymentLabel.TabIndex = 8;
            isReadyForPaymentLabel.Text = "Is Ready For Payment:";
            // 
            // feesLabel1
            // 
            this.feesLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.Fees", true));
            this.feesLabel1.Location = new System.Drawing.Point(497, 93);
            this.feesLabel1.Name = "feesLabel1";
            this.feesLabel1.Size = new System.Drawing.Size(100, 23);
            this.feesLabel1.TabIndex = 7;
            this.feesLabel1.Text = "label1";
            // 
            // feesLabel
            // 
            feesLabel.AutoSize = true;
            feesLabel.Location = new System.Drawing.Point(377, 93);
            feesLabel.Name = "feesLabel";
            feesLabel.Size = new System.Drawing.Size(33, 13);
            feesLabel.TabIndex = 6;
            feesLabel.Text = "Fees:";
            // 
            // dueDateLabel1
            // 
            this.dueDateLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.DueDate", true));
            this.dueDateLabel1.Location = new System.Drawing.Point(126, 46);
            this.dueDateLabel1.Name = "dueDateLabel1";
            this.dueDateLabel1.Size = new System.Drawing.Size(141, 23);
            this.dueDateLabel1.TabIndex = 5;
            this.dueDateLabel1.Text = "label1";
            // 
            // dueDateLabel
            // 
            dueDateLabel.AutoSize = true;
            dueDateLabel.Location = new System.Drawing.Point(6, 46);
            dueDateLabel.Name = "dueDateLabel";
            dueDateLabel.Size = new System.Drawing.Size(56, 13);
            dueDateLabel.TabIndex = 4;
            dueDateLabel.Text = "Due Date:";
            // 
            // dateLabel1
            // 
            this.dateLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.Date", true));
            this.dateLabel1.Location = new System.Drawing.Point(126, 23);
            this.dateLabel1.Name = "dateLabel1";
            this.dateLabel1.Size = new System.Drawing.Size(141, 23);
            this.dateLabel1.TabIndex = 3;
            this.dateLabel1.Text = "label1";
            // 
            // dateLabel
            // 
            dateLabel.AutoSize = true;
            dateLabel.Location = new System.Drawing.Point(6, 23);
            dateLabel.Name = "dateLabel";
            dateLabel.Size = new System.Drawing.Size(33, 13);
            dateLabel.TabIndex = 2;
            dateLabel.Text = "Date:";
            // 
            // calcCountLabel1
            // 
            this.calcCountLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Info.CalcCount", true));
            this.calcCountLabel1.Location = new System.Drawing.Point(497, 23);
            this.calcCountLabel1.Name = "calcCountLabel1";
            this.calcCountLabel1.Size = new System.Drawing.Size(100, 23);
            this.calcCountLabel1.TabIndex = 1;
            this.calcCountLabel1.Text = "label1";
            // 
            // calcCountLabel
            // 
            calcCountLabel.AutoSize = true;
            calcCountLabel.Location = new System.Drawing.Point(377, 23);
            calcCountLabel.Name = "calcCountLabel";
            calcCountLabel.Size = new System.Drawing.Size(62, 13);
            calcCountLabel.TabIndex = 0;
            calcCountLabel.Text = "Calc Count:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSaveChanges);
            this.groupBox2.Controls.Add(salaryDateLabel2);
            this.groupBox2.Controls.Add(this.salaryDateDateTimePicker);
            this.groupBox2.Controls.Add(daysCountLabel);
            this.groupBox2.Controls.Add(this.daysCountNumericUpDown);
            this.groupBox2.Controls.Add(endLabel);
            this.groupBox2.Controls.Add(this.endDateTimePicker);
            this.groupBox2.Controls.Add(startLabel);
            this.groupBox2.Controls.Add(this.startDateTimePicker);
            this.groupBox2.Controls.Add(noUpdateFromEmploymentLabel);
            this.groupBox2.Controls.Add(this.noUpdateFromEmploymentCheckBox);
            this.groupBox2.Controls.Add(titleLabel);
            this.groupBox2.Controls.Add(this.titleTextBox);
            this.groupBox2.Location = new System.Drawing.Point(6, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(717, 150);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Basic info";
            // 
            // titleTextBox
            // 
            this.titleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Input.Title", true));
            this.titleTextBox.Location = new System.Drawing.Point(163, 23);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(200, 20);
            this.titleTextBox.TabIndex = 7;
            // 
            // titleLabel
            // 
            titleLabel.AutoSize = true;
            titleLabel.Location = new System.Drawing.Point(9, 26);
            titleLabel.Name = "titleLabel";
            titleLabel.Size = new System.Drawing.Size(30, 13);
            titleLabel.TabIndex = 6;
            titleLabel.Text = "Title:";
            // 
            // noUpdateFromEmploymentCheckBox
            // 
            this.noUpdateFromEmploymentCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.payrollDetailsBindingSource, "Input.NoUpdateFromEmployment", true));
            this.noUpdateFromEmploymentCheckBox.Location = new System.Drawing.Point(163, 72);
            this.noUpdateFromEmploymentCheckBox.Name = "noUpdateFromEmploymentCheckBox";
            this.noUpdateFromEmploymentCheckBox.Size = new System.Drawing.Size(104, 24);
            this.noUpdateFromEmploymentCheckBox.TabIndex = 1;
            this.noUpdateFromEmploymentCheckBox.UseVisualStyleBackColor = true;
            // 
            // noUpdateFromEmploymentLabel
            // 
            noUpdateFromEmploymentLabel.AutoSize = true;
            noUpdateFromEmploymentLabel.Location = new System.Drawing.Point(9, 77);
            noUpdateFromEmploymentLabel.Name = "noUpdateFromEmploymentLabel";
            noUpdateFromEmploymentLabel.Size = new System.Drawing.Size(148, 13);
            noUpdateFromEmploymentLabel.TabIndex = 0;
            noUpdateFromEmploymentLabel.Text = "No Update From Employment:";
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Input.Period.Start", true));
            this.startDateTimePicker.Location = new System.Drawing.Point(500, 23);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.startDateTimePicker.TabIndex = 9;
            // 
            // startLabel
            // 
            startLabel.AutoSize = true;
            startLabel.Location = new System.Drawing.Point(377, 26);
            startLabel.Name = "startLabel";
            startLabel.Size = new System.Drawing.Size(32, 13);
            startLabel.TabIndex = 8;
            startLabel.Text = "Start:";
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Input.Period.End", true));
            this.endDateTimePicker.Location = new System.Drawing.Point(500, 49);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.endDateTimePicker.TabIndex = 11;
            // 
            // endLabel
            // 
            endLabel.AutoSize = true;
            endLabel.Location = new System.Drawing.Point(377, 52);
            endLabel.Name = "endLabel";
            endLabel.Size = new System.Drawing.Size(29, 13);
            endLabel.TabIndex = 10;
            endLabel.Text = "End:";
            // 
            // daysCountNumericUpDown
            // 
            this.daysCountNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.payrollDetailsBindingSource, "Input.Period.DaysCount", true));
            this.daysCountNumericUpDown.Location = new System.Drawing.Point(500, 76);
            this.daysCountNumericUpDown.Name = "daysCountNumericUpDown";
            this.daysCountNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.daysCountNumericUpDown.TabIndex = 12;
            // 
            // daysCountLabel
            // 
            daysCountLabel.AutoSize = true;
            daysCountLabel.Location = new System.Drawing.Point(377, 78);
            daysCountLabel.Name = "daysCountLabel";
            daysCountLabel.Size = new System.Drawing.Size(65, 13);
            daysCountLabel.TabIndex = 11;
            daysCountLabel.Text = "Days Count:";
            // 
            // salaryDateDateTimePicker
            // 
            this.salaryDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.payrollDetailsBindingSource, "Input.SalaryDate", true));
            this.salaryDateDateTimePicker.Location = new System.Drawing.Point(163, 51);
            this.salaryDateDateTimePicker.Name = "salaryDateDateTimePicker";
            this.salaryDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.salaryDateDateTimePicker.TabIndex = 13;
            // 
            // salaryDateLabel2
            // 
            salaryDateLabel2.AutoSize = true;
            salaryDateLabel2.Location = new System.Drawing.Point(9, 55);
            salaryDateLabel2.Name = "salaryDateLabel2";
            salaryDateLabel2.Size = new System.Drawing.Size(65, 13);
            salaryDateLabel2.TabIndex = 12;
            salaryDateLabel2.Text = "Salary Date:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 314);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(539, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "TODO, missing in UI: Editable dates, Calculations (array) + Enum controls (WageBa" +
    "ses, Status, PaymentChannel)";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabOverview);
            this.tabControl.Controls.Add(this.tabDetails);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(959, 554);
            this.tabControl.TabIndex = 0;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            // 
            // payrollDetailsBindingSource
            // 
            this.payrollDetailsBindingSource.DataSource = typeof(Salaxy.PayrollDetails);
            this.payrollDetailsBindingSource.CurrentChanged += new System.EventHandler(this.payrollDetailsBindingSource_CurrentChanged);
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Location = new System.Drawing.Point(500, 103);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(120, 23);
            this.btnSaveChanges.TabIndex = 14;
            this.btnSaveChanges.Text = "Save changes";
            this.btnSaveChanges.UseVisualStyleBackColor = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // PayrollControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "PayrollControl";
            this.Size = new System.Drawing.Size(959, 579);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.webView)).EndInit();
            this.tabOverview.ResumeLayout(false);
            this.tabOverview.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daysCountNumericUpDown)).EndInit();
            this.tabControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.payrollDetailsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnImport;
        private System.Windows.Forms.ToolStripButton btnCheckPersonalIds;
        private System.Windows.Forms.BindingSource payrollDetailsBindingSource;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabOverview;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker salaryDateDateTimePicker;
        private System.Windows.Forms.NumericUpDown daysCountNumericUpDown;
        private System.Windows.Forms.DateTimePicker endDateTimePicker;
        private System.Windows.Forms.DateTimePicker startDateTimePicker;
        private System.Windows.Forms.CheckBox noUpdateFromEmploymentCheckBox;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label calcCountLabel1;
        private System.Windows.Forms.Label dateLabel1;
        private System.Windows.Forms.Label dueDateLabel1;
        private System.Windows.Forms.Label feesLabel1;
        private System.Windows.Forms.Label isReadyForPaymentLabel1;
        private System.Windows.Forms.Label paymentDateLabel1;
        private System.Windows.Forms.Label salaryDateLabel1;
        private System.Windows.Forms.Label totalGrossSalaryLabel1;
        private System.Windows.Forms.Label totalPaymentLabel1;
        private System.Windows.Forms.TabPage tabDetails;
        private Microsoft.Web.WebView2.WinForms.WebView2 webView;
        private System.Windows.Forms.Button btnSaveChanges;
    }
}
