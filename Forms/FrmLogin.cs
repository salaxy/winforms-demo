﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }


        private void frmLogin_Load(object sender, EventArgs e)
        {
            // Environment values in combo box.
            Dictionary<string, string> environments = new Dictionary<string, string>();
            environments.Add("Demo", "https://demo-secure.salaxy.com");
            environments.Add("Testi", "https://test-secure.salaxy.com");
            environments.Add("Localhost (82)", "http://localhost:82");
            environments.Add("Tuotanto", "https://secure.salaxy.com");
            cbxEnvironments.DataSource = new BindingSource(environments, null);
            cbxEnvironments.DisplayMember = "Key";
            cbxEnvironments.ValueMember = "Value";
            if (!String.IsNullOrWhiteSpace(Properties.Settings.Default.UserName))
            {
                txtUid.Text = Properties.Settings.Default.UserName;
                txtPwd.Text = Properties.Settings.Default.Password;
                cbxEnvironments.SelectedItem = environments.FirstOrDefault(x => x.Value == Properties.Settings.Default.Server);
            }
        }
    }
}
