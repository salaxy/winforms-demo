﻿using Salaxy;
using SalaxyDemo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    public partial class FrmPayroll : Form
    {
        public FrmPayroll()
        {
            InitializeComponent();
        }

        public void SetPayroll(PayrollDetails item, SalaxyApiHelper api)
        {
            payrollControl.Api = api;
            payrollControl.Payroll = item;
            Text = "Payroll: " + item.Id;
        }

        private void FrmPayroll_Load(object sender, EventArgs e)
        {

        }
    }
}
