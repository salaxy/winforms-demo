﻿
namespace SalaxyDemo.Forms
{
    partial class CalculatorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label backofficeNotesLabel;
            System.Windows.Forms.Label costCenterLabel;
            System.Windows.Forms.Label occupationCodeLabel;
            System.Windows.Forms.Label paymentIdLabel;
            System.Windows.Forms.Label payrollIdLabel;
            System.Windows.Forms.Label pensionPaymentDateLabel;
            System.Windows.Forms.Label pensionPaymentRefLabel;
            System.Windows.Forms.Label pensionPaymentSpecifierLabel;
            System.Windows.Forms.Label projectNumberLabel;
            System.Windows.Forms.Label salarySlipMessageLabel;
            System.Windows.Forms.Label workDescriptionLabel;
            System.Windows.Forms.Label workEndDateLabel;
            System.Windows.Forms.Label workerMessageLabel;
            System.Windows.Forms.Label workStartDateLabel;
            System.Windows.Forms.Label emailLabel;
            System.Windows.Forms.Label ibanNumberLabel;
            System.Windows.Forms.Label socialSecurityNumberValidLabel;
            System.Windows.Forms.Label telephoneLabel;
            System.Windows.Forms.Label accountIdLabel;
            System.Windows.Forms.Label displayNameLabel;
            System.Windows.Forms.Label isReadOnlyLabel;
            System.Windows.Forms.Label employmentIdLabel;
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label idLabel2;
            System.Windows.Forms.Label createdAtLabel;
            System.Windows.Forms.Label workStartDateLabel1;
            System.Windows.Forms.Label workEndDateLabel1;
            System.Windows.Forms.Label occupationCodeLabel1;
            System.Windows.Forms.Label costCenterLabel1;
            System.Windows.Forms.Label pensionLabel;
            System.Windows.Forms.Label totalLabel;
            System.Windows.Forms.Label totalAccidentInsuranceBaseLabel;
            System.Windows.Forms.Label totalBaseSalaryLabel;
            System.Windows.Forms.Label totalExpensesLabel;
            System.Windows.Forms.Label totalGrossSalaryLabel;
            System.Windows.Forms.Label totalHealthInsuranceBaseLabel;
            System.Windows.Forms.Label totalPayableLabel;
            System.Windows.Forms.Label totalPensionInsuranceBaseLabel;
            System.Windows.Forms.Label totalSocialSecurityBaseLabel;
            System.Windows.Forms.Label totalTaxableLabel;
            System.Windows.Forms.Label totalUnemploymentInsuranceBaseLabel;
            System.Windows.Forms.Label unemploymentLabel;
            System.Windows.Forms.Label allSideCostsLabel;
            System.Windows.Forms.Label deductionForeclosureLabel;
            System.Windows.Forms.Label deductionOtherDeductionsLabel;
            System.Windows.Forms.Label deductionPensionSelfPaymentLabel;
            System.Windows.Forms.Label deductionSalaryAdvanceLabel;
            System.Windows.Forms.Label deductionTaxAndSocialSecuritySelfPaymentLabel;
            System.Windows.Forms.Label deductionUnemploymentSelfPaymentLabel;
            System.Windows.Forms.Label deductionUnionPaymentLabel;
            System.Windows.Forms.Label deductionWorkerSelfPaymentLabel;
            System.Windows.Forms.Label finalCostLabel;
            System.Windows.Forms.Label foreclosureByPalkkausLabel;
            System.Windows.Forms.Label householdDeductionLabel;
            System.Windows.Forms.Label mandatorySideCostsLabel;
            System.Windows.Forms.Label palkkausLabel;
            System.Windows.Forms.Label pensionLabel2;
            System.Windows.Forms.Label socialSecurityLabel;
            System.Windows.Forms.Label totalDeductionsLabel;
            System.Windows.Forms.Label totalPaymentLabel;
            System.Windows.Forms.Label totalPaymentLegacyLabel;
            System.Windows.Forms.Label totalSalaryCostLabel;
            System.Windows.Forms.Label unemploymentLabel2;
            System.Windows.Forms.Label benefitsLabel;
            System.Windows.Forms.Label deductionsLabel;
            System.Windows.Forms.Label foreclosureLabel;
            System.Windows.Forms.Label fullOtherDeductionsLabel;
            System.Windows.Forms.Label fullPensionLabel;
            System.Windows.Forms.Label fullPrepaidExpensesLabel;
            System.Windows.Forms.Label fullSalaryAdvanceLabel;
            System.Windows.Forms.Label fullTaxLabel;
            System.Windows.Forms.Label fullUnemploymentInsuranceLabel;
            System.Windows.Forms.Label fullUnionPaymentLabel;
            System.Windows.Forms.Label otherDeductionsLabel;
            System.Windows.Forms.Label pensionLabel4;
            System.Windows.Forms.Label prepaidExpensesLabel;
            System.Windows.Forms.Label salaryAdvanceLabel;
            System.Windows.Forms.Label salaryAfterTaxLabel;
            System.Windows.Forms.Label salaryAfterTaxAndForeclosureLabel;
            System.Windows.Forms.Label salaryPaymentLabel;
            System.Windows.Forms.Label taxLabel;
            System.Windows.Forms.Label totalWorkerPaymentLabel;
            System.Windows.Forms.Label totalWorkerPaymentLegacyLabel;
            System.Windows.Forms.Label unemploymentInsuranceLabel;
            System.Windows.Forms.Label unionPaymentLabel;
            System.Windows.Forms.Label workerSideCostsLabel;
            this.tabsControl = new System.Windows.Forms.TabControl();
            this.tabOverview = new System.Windows.Forms.TabPage();
            this.btnAddRow = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRecalculate = new System.Windows.Forms.Button();
            this.workEndDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.workStartDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.createdAtLabel1 = new System.Windows.Forms.Label();
            this.gridRows = new System.Windows.Forms.DataGridView();
            this.idLabel3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.costCenterTextBox1 = new System.Windows.Forms.TextBox();
            this.occupationCodeTextBox1 = new System.Windows.Forms.TextBox();
            this.idLabel1 = new System.Windows.Forms.Label();
            this.employmentIdLabel1 = new System.Windows.Forms.Label();
            this.isReadOnlyLabel1 = new System.Windows.Forms.Label();
            this.displayNameLabel1 = new System.Windows.Forms.Label();
            this.accountIdLabel1 = new System.Windows.Forms.Label();
            this.emailLabel1 = new System.Windows.Forms.Label();
            this.ibanNumberLabel1 = new System.Windows.Forms.Label();
            this.socialSecurityNumberValidLabel1 = new System.Windows.Forms.Label();
            this.telephoneLabel1 = new System.Windows.Forms.Label();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.backofficeNotesTextBox = new System.Windows.Forms.TextBox();
            this.costCenterTextBox = new System.Windows.Forms.TextBox();
            this.occupationCodeTextBox = new System.Windows.Forms.TextBox();
            this.paymentIdTextBox = new System.Windows.Forms.TextBox();
            this.payrollIdTextBox = new System.Windows.Forms.TextBox();
            this.pensionPaymentDateTextBox = new System.Windows.Forms.TextBox();
            this.pensionPaymentRefTextBox = new System.Windows.Forms.TextBox();
            this.pensionPaymentSpecifierTextBox = new System.Windows.Forms.TextBox();
            this.projectNumberTextBox = new System.Windows.Forms.TextBox();
            this.salarySlipMessageTextBox = new System.Windows.Forms.TextBox();
            this.workDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.workEndDateTextBox = new System.Windows.Forms.TextBox();
            this.workerMessageTextBox = new System.Windows.Forms.TextBox();
            this.workStartDateTextBox = new System.Windows.Forms.TextBox();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.benefitsLabel1 = new System.Windows.Forms.Label();
            this.deductionsLabel1 = new System.Windows.Forms.Label();
            this.foreclosureLabel1 = new System.Windows.Forms.Label();
            this.fullOtherDeductionsLabel1 = new System.Windows.Forms.Label();
            this.fullPensionLabel1 = new System.Windows.Forms.Label();
            this.fullPrepaidExpensesLabel1 = new System.Windows.Forms.Label();
            this.fullSalaryAdvanceLabel1 = new System.Windows.Forms.Label();
            this.fullTaxLabel1 = new System.Windows.Forms.Label();
            this.fullUnemploymentInsuranceLabel1 = new System.Windows.Forms.Label();
            this.fullUnionPaymentLabel1 = new System.Windows.Forms.Label();
            this.otherDeductionsLabel1 = new System.Windows.Forms.Label();
            this.pensionLabel5 = new System.Windows.Forms.Label();
            this.prepaidExpensesLabel1 = new System.Windows.Forms.Label();
            this.salaryAdvanceLabel1 = new System.Windows.Forms.Label();
            this.salaryAfterTaxLabel1 = new System.Windows.Forms.Label();
            this.salaryAfterTaxAndForeclosureLabel1 = new System.Windows.Forms.Label();
            this.salaryPaymentLabel1 = new System.Windows.Forms.Label();
            this.taxLabel1 = new System.Windows.Forms.Label();
            this.totalWorkerPaymentLabel1 = new System.Windows.Forms.Label();
            this.totalWorkerPaymentLegacyLabel1 = new System.Windows.Forms.Label();
            this.unemploymentInsuranceLabel1 = new System.Windows.Forms.Label();
            this.unionPaymentLabel1 = new System.Windows.Forms.Label();
            this.workerSideCostsLabel1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.allSideCostsLabel1 = new System.Windows.Forms.Label();
            this.deductionForeclosureLabel1 = new System.Windows.Forms.Label();
            this.deductionOtherDeductionsLabel1 = new System.Windows.Forms.Label();
            this.deductionPensionSelfPaymentLabel1 = new System.Windows.Forms.Label();
            this.deductionSalaryAdvanceLabel1 = new System.Windows.Forms.Label();
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1 = new System.Windows.Forms.Label();
            this.deductionUnemploymentSelfPaymentLabel1 = new System.Windows.Forms.Label();
            this.deductionUnionPaymentLabel1 = new System.Windows.Forms.Label();
            this.deductionWorkerSelfPaymentLabel1 = new System.Windows.Forms.Label();
            this.finalCostLabel1 = new System.Windows.Forms.Label();
            this.foreclosureByPalkkausLabel1 = new System.Windows.Forms.Label();
            this.householdDeductionLabel1 = new System.Windows.Forms.Label();
            this.mandatorySideCostsLabel1 = new System.Windows.Forms.Label();
            this.palkkausLabel1 = new System.Windows.Forms.Label();
            this.pensionLabel3 = new System.Windows.Forms.Label();
            this.socialSecurityLabel1 = new System.Windows.Forms.Label();
            this.totalDeductionsLabel1 = new System.Windows.Forms.Label();
            this.totalPaymentLabel1 = new System.Windows.Forms.Label();
            this.totalPaymentLegacyLabel1 = new System.Windows.Forms.Label();
            this.totalSalaryCostLabel1 = new System.Windows.Forms.Label();
            this.unemploymentLabel3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pensionLabel1 = new System.Windows.Forms.Label();
            this.totalLabel1 = new System.Windows.Forms.Label();
            this.totalAccidentInsuranceBaseLabel1 = new System.Windows.Forms.Label();
            this.totalBaseSalaryLabel1 = new System.Windows.Forms.Label();
            this.totalExpensesLabel1 = new System.Windows.Forms.Label();
            this.totalGrossSalaryLabel1 = new System.Windows.Forms.Label();
            this.totalHealthInsuranceBaseLabel1 = new System.Windows.Forms.Label();
            this.totalPayableLabel1 = new System.Windows.Forms.Label();
            this.totalPensionInsuranceBaseLabel1 = new System.Windows.Forms.Label();
            this.totalSocialSecurityBaseLabel1 = new System.Windows.Forms.Label();
            this.totalTaxableLabel1 = new System.Windows.Forms.Label();
            this.totalUnemploymentInsuranceBaseLabel1 = new System.Windows.Forms.Label();
            this.unemploymentLabel1 = new System.Windows.Forms.Label();
            this.calculationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            backofficeNotesLabel = new System.Windows.Forms.Label();
            costCenterLabel = new System.Windows.Forms.Label();
            occupationCodeLabel = new System.Windows.Forms.Label();
            paymentIdLabel = new System.Windows.Forms.Label();
            payrollIdLabel = new System.Windows.Forms.Label();
            pensionPaymentDateLabel = new System.Windows.Forms.Label();
            pensionPaymentRefLabel = new System.Windows.Forms.Label();
            pensionPaymentSpecifierLabel = new System.Windows.Forms.Label();
            projectNumberLabel = new System.Windows.Forms.Label();
            salarySlipMessageLabel = new System.Windows.Forms.Label();
            workDescriptionLabel = new System.Windows.Forms.Label();
            workEndDateLabel = new System.Windows.Forms.Label();
            workerMessageLabel = new System.Windows.Forms.Label();
            workStartDateLabel = new System.Windows.Forms.Label();
            emailLabel = new System.Windows.Forms.Label();
            ibanNumberLabel = new System.Windows.Forms.Label();
            socialSecurityNumberValidLabel = new System.Windows.Forms.Label();
            telephoneLabel = new System.Windows.Forms.Label();
            accountIdLabel = new System.Windows.Forms.Label();
            displayNameLabel = new System.Windows.Forms.Label();
            isReadOnlyLabel = new System.Windows.Forms.Label();
            employmentIdLabel = new System.Windows.Forms.Label();
            idLabel = new System.Windows.Forms.Label();
            idLabel2 = new System.Windows.Forms.Label();
            createdAtLabel = new System.Windows.Forms.Label();
            workStartDateLabel1 = new System.Windows.Forms.Label();
            workEndDateLabel1 = new System.Windows.Forms.Label();
            occupationCodeLabel1 = new System.Windows.Forms.Label();
            costCenterLabel1 = new System.Windows.Forms.Label();
            pensionLabel = new System.Windows.Forms.Label();
            totalLabel = new System.Windows.Forms.Label();
            totalAccidentInsuranceBaseLabel = new System.Windows.Forms.Label();
            totalBaseSalaryLabel = new System.Windows.Forms.Label();
            totalExpensesLabel = new System.Windows.Forms.Label();
            totalGrossSalaryLabel = new System.Windows.Forms.Label();
            totalHealthInsuranceBaseLabel = new System.Windows.Forms.Label();
            totalPayableLabel = new System.Windows.Forms.Label();
            totalPensionInsuranceBaseLabel = new System.Windows.Forms.Label();
            totalSocialSecurityBaseLabel = new System.Windows.Forms.Label();
            totalTaxableLabel = new System.Windows.Forms.Label();
            totalUnemploymentInsuranceBaseLabel = new System.Windows.Forms.Label();
            unemploymentLabel = new System.Windows.Forms.Label();
            allSideCostsLabel = new System.Windows.Forms.Label();
            deductionForeclosureLabel = new System.Windows.Forms.Label();
            deductionOtherDeductionsLabel = new System.Windows.Forms.Label();
            deductionPensionSelfPaymentLabel = new System.Windows.Forms.Label();
            deductionSalaryAdvanceLabel = new System.Windows.Forms.Label();
            deductionTaxAndSocialSecuritySelfPaymentLabel = new System.Windows.Forms.Label();
            deductionUnemploymentSelfPaymentLabel = new System.Windows.Forms.Label();
            deductionUnionPaymentLabel = new System.Windows.Forms.Label();
            deductionWorkerSelfPaymentLabel = new System.Windows.Forms.Label();
            finalCostLabel = new System.Windows.Forms.Label();
            foreclosureByPalkkausLabel = new System.Windows.Forms.Label();
            householdDeductionLabel = new System.Windows.Forms.Label();
            mandatorySideCostsLabel = new System.Windows.Forms.Label();
            palkkausLabel = new System.Windows.Forms.Label();
            pensionLabel2 = new System.Windows.Forms.Label();
            socialSecurityLabel = new System.Windows.Forms.Label();
            totalDeductionsLabel = new System.Windows.Forms.Label();
            totalPaymentLabel = new System.Windows.Forms.Label();
            totalPaymentLegacyLabel = new System.Windows.Forms.Label();
            totalSalaryCostLabel = new System.Windows.Forms.Label();
            unemploymentLabel2 = new System.Windows.Forms.Label();
            benefitsLabel = new System.Windows.Forms.Label();
            deductionsLabel = new System.Windows.Forms.Label();
            foreclosureLabel = new System.Windows.Forms.Label();
            fullOtherDeductionsLabel = new System.Windows.Forms.Label();
            fullPensionLabel = new System.Windows.Forms.Label();
            fullPrepaidExpensesLabel = new System.Windows.Forms.Label();
            fullSalaryAdvanceLabel = new System.Windows.Forms.Label();
            fullTaxLabel = new System.Windows.Forms.Label();
            fullUnemploymentInsuranceLabel = new System.Windows.Forms.Label();
            fullUnionPaymentLabel = new System.Windows.Forms.Label();
            otherDeductionsLabel = new System.Windows.Forms.Label();
            pensionLabel4 = new System.Windows.Forms.Label();
            prepaidExpensesLabel = new System.Windows.Forms.Label();
            salaryAdvanceLabel = new System.Windows.Forms.Label();
            salaryAfterTaxLabel = new System.Windows.Forms.Label();
            salaryAfterTaxAndForeclosureLabel = new System.Windows.Forms.Label();
            salaryPaymentLabel = new System.Windows.Forms.Label();
            taxLabel = new System.Windows.Forms.Label();
            totalWorkerPaymentLabel = new System.Windows.Forms.Label();
            totalWorkerPaymentLegacyLabel = new System.Windows.Forms.Label();
            unemploymentInsuranceLabel = new System.Windows.Forms.Label();
            unionPaymentLabel = new System.Windows.Forms.Label();
            workerSideCostsLabel = new System.Windows.Forms.Label();
            this.tabsControl.SuspendLayout();
            this.tabOverview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRows)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabInfo.SuspendLayout();
            this.tabResults.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calculationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // backofficeNotesLabel
            // 
            backofficeNotesLabel.AutoSize = true;
            backofficeNotesLabel.Location = new System.Drawing.Point(6, 12);
            backofficeNotesLabel.Name = "backofficeNotesLabel";
            backofficeNotesLabel.Size = new System.Drawing.Size(92, 13);
            backofficeNotesLabel.TabIndex = 0;
            backofficeNotesLabel.Text = "Backoffice Notes:";
            // 
            // costCenterLabel
            // 
            costCenterLabel.AutoSize = true;
            costCenterLabel.Location = new System.Drawing.Point(6, 38);
            costCenterLabel.Name = "costCenterLabel";
            costCenterLabel.Size = new System.Drawing.Size(65, 13);
            costCenterLabel.TabIndex = 2;
            costCenterLabel.Text = "Cost Center:";
            // 
            // occupationCodeLabel
            // 
            occupationCodeLabel.AutoSize = true;
            occupationCodeLabel.Location = new System.Drawing.Point(6, 64);
            occupationCodeLabel.Name = "occupationCodeLabel";
            occupationCodeLabel.Size = new System.Drawing.Size(93, 13);
            occupationCodeLabel.TabIndex = 4;
            occupationCodeLabel.Text = "Occupation Code:";
            // 
            // paymentIdLabel
            // 
            paymentIdLabel.AutoSize = true;
            paymentIdLabel.Location = new System.Drawing.Point(6, 90);
            paymentIdLabel.Name = "paymentIdLabel";
            paymentIdLabel.Size = new System.Drawing.Size(63, 13);
            paymentIdLabel.TabIndex = 6;
            paymentIdLabel.Text = "Payment Id:";
            // 
            // payrollIdLabel
            // 
            payrollIdLabel.AutoSize = true;
            payrollIdLabel.Location = new System.Drawing.Point(6, 116);
            payrollIdLabel.Name = "payrollIdLabel";
            payrollIdLabel.Size = new System.Drawing.Size(53, 13);
            payrollIdLabel.TabIndex = 8;
            payrollIdLabel.Text = "Payroll Id:";
            // 
            // pensionPaymentDateLabel
            // 
            pensionPaymentDateLabel.AutoSize = true;
            pensionPaymentDateLabel.Location = new System.Drawing.Point(6, 142);
            pensionPaymentDateLabel.Name = "pensionPaymentDateLabel";
            pensionPaymentDateLabel.Size = new System.Drawing.Size(118, 13);
            pensionPaymentDateLabel.TabIndex = 10;
            pensionPaymentDateLabel.Text = "Pension Payment Date:";
            // 
            // pensionPaymentRefLabel
            // 
            pensionPaymentRefLabel.AutoSize = true;
            pensionPaymentRefLabel.Location = new System.Drawing.Point(6, 168);
            pensionPaymentRefLabel.Name = "pensionPaymentRefLabel";
            pensionPaymentRefLabel.Size = new System.Drawing.Size(112, 13);
            pensionPaymentRefLabel.TabIndex = 12;
            pensionPaymentRefLabel.Text = "Pension Payment Ref:";
            // 
            // pensionPaymentSpecifierLabel
            // 
            pensionPaymentSpecifierLabel.AutoSize = true;
            pensionPaymentSpecifierLabel.Location = new System.Drawing.Point(6, 194);
            pensionPaymentSpecifierLabel.Name = "pensionPaymentSpecifierLabel";
            pensionPaymentSpecifierLabel.Size = new System.Drawing.Size(136, 13);
            pensionPaymentSpecifierLabel.TabIndex = 14;
            pensionPaymentSpecifierLabel.Text = "Pension Payment Specifier:";
            // 
            // projectNumberLabel
            // 
            projectNumberLabel.AutoSize = true;
            projectNumberLabel.Location = new System.Drawing.Point(6, 220);
            projectNumberLabel.Name = "projectNumberLabel";
            projectNumberLabel.Size = new System.Drawing.Size(83, 13);
            projectNumberLabel.TabIndex = 16;
            projectNumberLabel.Text = "Project Number:";
            // 
            // salarySlipMessageLabel
            // 
            salarySlipMessageLabel.AutoSize = true;
            salarySlipMessageLabel.Location = new System.Drawing.Point(6, 246);
            salarySlipMessageLabel.Name = "salarySlipMessageLabel";
            salarySlipMessageLabel.Size = new System.Drawing.Size(105, 13);
            salarySlipMessageLabel.TabIndex = 18;
            salarySlipMessageLabel.Text = "Salary Slip Message:";
            // 
            // workDescriptionLabel
            // 
            workDescriptionLabel.AutoSize = true;
            workDescriptionLabel.Location = new System.Drawing.Point(6, 272);
            workDescriptionLabel.Name = "workDescriptionLabel";
            workDescriptionLabel.Size = new System.Drawing.Size(92, 13);
            workDescriptionLabel.TabIndex = 20;
            workDescriptionLabel.Text = "Work Description:";
            // 
            // workEndDateLabel
            // 
            workEndDateLabel.AutoSize = true;
            workEndDateLabel.Location = new System.Drawing.Point(6, 298);
            workEndDateLabel.Name = "workEndDateLabel";
            workEndDateLabel.Size = new System.Drawing.Size(84, 13);
            workEndDateLabel.TabIndex = 22;
            workEndDateLabel.Text = "Work End Date:";
            // 
            // workerMessageLabel
            // 
            workerMessageLabel.AutoSize = true;
            workerMessageLabel.Location = new System.Drawing.Point(6, 324);
            workerMessageLabel.Name = "workerMessageLabel";
            workerMessageLabel.Size = new System.Drawing.Size(91, 13);
            workerMessageLabel.TabIndex = 24;
            workerMessageLabel.Text = "Worker Message:";
            // 
            // workStartDateLabel
            // 
            workStartDateLabel.AutoSize = true;
            workStartDateLabel.Location = new System.Drawing.Point(6, 350);
            workStartDateLabel.Name = "workStartDateLabel";
            workStartDateLabel.Size = new System.Drawing.Size(87, 13);
            workStartDateLabel.TabIndex = 26;
            workStartDateLabel.Text = "Work Start Date:";
            // 
            // emailLabel
            // 
            emailLabel.AutoSize = true;
            emailLabel.Location = new System.Drawing.Point(347, 23);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new System.Drawing.Size(35, 13);
            emailLabel.TabIndex = 0;
            emailLabel.Text = "Email:";
            // 
            // ibanNumberLabel
            // 
            ibanNumberLabel.AutoSize = true;
            ibanNumberLabel.Location = new System.Drawing.Point(311, 46);
            ibanNumberLabel.Name = "ibanNumberLabel";
            ibanNumberLabel.Size = new System.Drawing.Size(71, 13);
            ibanNumberLabel.TabIndex = 4;
            ibanNumberLabel.Text = "Iban Number:";
            // 
            // socialSecurityNumberValidLabel
            // 
            socialSecurityNumberValidLabel.AutoSize = true;
            socialSecurityNumberValidLabel.Location = new System.Drawing.Point(6, 115);
            socialSecurityNumberValidLabel.Name = "socialSecurityNumberValidLabel";
            socialSecurityNumberValidLabel.Size = new System.Drawing.Size(104, 13);
            socialSecurityNumberValidLabel.TabIndex = 10;
            socialSecurityNumberValidLabel.Text = "Personal ID (HETU):";
            // 
            // telephoneLabel
            // 
            telephoneLabel.AutoSize = true;
            telephoneLabel.Location = new System.Drawing.Point(321, 69);
            telephoneLabel.Name = "telephoneLabel";
            telephoneLabel.Size = new System.Drawing.Size(61, 13);
            telephoneLabel.TabIndex = 12;
            telephoneLabel.Text = "Telephone:";
            // 
            // accountIdLabel
            // 
            accountIdLabel.AutoSize = true;
            accountIdLabel.Location = new System.Drawing.Point(48, 23);
            accountIdLabel.Name = "accountIdLabel";
            accountIdLabel.Size = new System.Drawing.Size(62, 13);
            accountIdLabel.TabIndex = 14;
            accountIdLabel.Text = "Account Id:";
            // 
            // displayNameLabel
            // 
            displayNameLabel.AutoSize = true;
            displayNameLabel.Location = new System.Drawing.Point(35, 69);
            displayNameLabel.Name = "displayNameLabel";
            displayNameLabel.Size = new System.Drawing.Size(75, 13);
            displayNameLabel.TabIndex = 16;
            displayNameLabel.Text = "Display Name:";
            // 
            // isReadOnlyLabel
            // 
            isReadOnlyLabel.AutoSize = true;
            isReadOnlyLabel.Location = new System.Drawing.Point(39, 92);
            isReadOnlyLabel.Name = "isReadOnlyLabel";
            isReadOnlyLabel.Size = new System.Drawing.Size(71, 13);
            isReadOnlyLabel.TabIndex = 18;
            isReadOnlyLabel.Text = "Is Read Only:";
            // 
            // employmentIdLabel
            // 
            employmentIdLabel.AutoSize = true;
            employmentIdLabel.Location = new System.Drawing.Point(31, 46);
            employmentIdLabel.Name = "employmentIdLabel";
            employmentIdLabel.Size = new System.Drawing.Size(79, 13);
            employmentIdLabel.TabIndex = 22;
            employmentIdLabel.Text = "Employment Id:";
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(8, -26);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(19, 13);
            idLabel.TabIndex = 2;
            idLabel.Text = "Id:";
            // 
            // idLabel2
            // 
            idLabel2.AutoSize = true;
            idLabel2.Location = new System.Drawing.Point(97, 12);
            idLabel2.Name = "idLabel2";
            idLabel2.Size = new System.Drawing.Size(19, 13);
            idLabel2.TabIndex = 2;
            idLabel2.Text = "Id:";
            // 
            // createdAtLabel
            // 
            createdAtLabel.AutoSize = true;
            createdAtLabel.Location = new System.Drawing.Point(56, 34);
            createdAtLabel.Name = "createdAtLabel";
            createdAtLabel.Size = new System.Drawing.Size(60, 13);
            createdAtLabel.TabIndex = 4;
            createdAtLabel.Text = "Created At:";
            // 
            // workStartDateLabel1
            // 
            workStartDateLabel1.AutoSize = true;
            workStartDateLabel1.Location = new System.Drawing.Point(301, 12);
            workStartDateLabel1.Name = "workStartDateLabel1";
            workStartDateLabel1.Size = new System.Drawing.Size(87, 13);
            workStartDateLabel1.TabIndex = 6;
            workStartDateLabel1.Text = "Work Start Date:";
            // 
            // workEndDateLabel1
            // 
            workEndDateLabel1.AutoSize = true;
            workEndDateLabel1.Location = new System.Drawing.Point(304, 34);
            workEndDateLabel1.Name = "workEndDateLabel1";
            workEndDateLabel1.Size = new System.Drawing.Size(84, 13);
            workEndDateLabel1.TabIndex = 8;
            workEndDateLabel1.Text = "Work End Date:";
            // 
            // occupationCodeLabel1
            // 
            occupationCodeLabel1.AutoSize = true;
            occupationCodeLabel1.Location = new System.Drawing.Point(289, 92);
            occupationCodeLabel1.Name = "occupationCodeLabel1";
            occupationCodeLabel1.Size = new System.Drawing.Size(93, 13);
            occupationCodeLabel1.TabIndex = 23;
            occupationCodeLabel1.Text = "Occupation Code:";
            // 
            // costCenterLabel1
            // 
            costCenterLabel1.AutoSize = true;
            costCenterLabel1.Location = new System.Drawing.Point(317, 115);
            costCenterLabel1.Name = "costCenterLabel1";
            costCenterLabel1.Size = new System.Drawing.Size(65, 13);
            costCenterLabel1.TabIndex = 24;
            costCenterLabel1.Text = "Cost Center:";
            // 
            // pensionLabel
            // 
            pensionLabel.AutoSize = true;
            pensionLabel.Location = new System.Drawing.Point(6, 16);
            pensionLabel.Name = "pensionLabel";
            pensionLabel.Size = new System.Drawing.Size(48, 13);
            pensionLabel.TabIndex = 0;
            pensionLabel.Text = "Pension:";
            // 
            // totalLabel
            // 
            totalLabel.AutoSize = true;
            totalLabel.Location = new System.Drawing.Point(6, 39);
            totalLabel.Name = "totalLabel";
            totalLabel.Size = new System.Drawing.Size(34, 13);
            totalLabel.TabIndex = 2;
            totalLabel.Text = "Total:";
            // 
            // totalAccidentInsuranceBaseLabel
            // 
            totalAccidentInsuranceBaseLabel.AutoSize = true;
            totalAccidentInsuranceBaseLabel.Location = new System.Drawing.Point(6, 62);
            totalAccidentInsuranceBaseLabel.Name = "totalAccidentInsuranceBaseLabel";
            totalAccidentInsuranceBaseLabel.Size = new System.Drawing.Size(156, 13);
            totalAccidentInsuranceBaseLabel.TabIndex = 4;
            totalAccidentInsuranceBaseLabel.Text = "Total Accident Insurance Base:";
            // 
            // totalBaseSalaryLabel
            // 
            totalBaseSalaryLabel.AutoSize = true;
            totalBaseSalaryLabel.Location = new System.Drawing.Point(6, 85);
            totalBaseSalaryLabel.Name = "totalBaseSalaryLabel";
            totalBaseSalaryLabel.Size = new System.Drawing.Size(93, 13);
            totalBaseSalaryLabel.TabIndex = 6;
            totalBaseSalaryLabel.Text = "Total Base Salary:";
            // 
            // totalExpensesLabel
            // 
            totalExpensesLabel.AutoSize = true;
            totalExpensesLabel.Location = new System.Drawing.Point(6, 108);
            totalExpensesLabel.Name = "totalExpensesLabel";
            totalExpensesLabel.Size = new System.Drawing.Size(83, 13);
            totalExpensesLabel.TabIndex = 8;
            totalExpensesLabel.Text = "Total Expenses:";
            // 
            // totalGrossSalaryLabel
            // 
            totalGrossSalaryLabel.AutoSize = true;
            totalGrossSalaryLabel.Location = new System.Drawing.Point(6, 131);
            totalGrossSalaryLabel.Name = "totalGrossSalaryLabel";
            totalGrossSalaryLabel.Size = new System.Drawing.Size(96, 13);
            totalGrossSalaryLabel.TabIndex = 10;
            totalGrossSalaryLabel.Text = "Total Gross Salary:";
            // 
            // totalHealthInsuranceBaseLabel
            // 
            totalHealthInsuranceBaseLabel.AutoSize = true;
            totalHealthInsuranceBaseLabel.Location = new System.Drawing.Point(6, 154);
            totalHealthInsuranceBaseLabel.Name = "totalHealthInsuranceBaseLabel";
            totalHealthInsuranceBaseLabel.Size = new System.Drawing.Size(145, 13);
            totalHealthInsuranceBaseLabel.TabIndex = 12;
            totalHealthInsuranceBaseLabel.Text = "Total Health Insurance Base:";
            // 
            // totalPayableLabel
            // 
            totalPayableLabel.AutoSize = true;
            totalPayableLabel.Location = new System.Drawing.Point(6, 177);
            totalPayableLabel.Name = "totalPayableLabel";
            totalPayableLabel.Size = new System.Drawing.Size(75, 13);
            totalPayableLabel.TabIndex = 14;
            totalPayableLabel.Text = "Total Payable:";
            // 
            // totalPensionInsuranceBaseLabel
            // 
            totalPensionInsuranceBaseLabel.AutoSize = true;
            totalPensionInsuranceBaseLabel.Location = new System.Drawing.Point(6, 200);
            totalPensionInsuranceBaseLabel.Name = "totalPensionInsuranceBaseLabel";
            totalPensionInsuranceBaseLabel.Size = new System.Drawing.Size(152, 13);
            totalPensionInsuranceBaseLabel.TabIndex = 16;
            totalPensionInsuranceBaseLabel.Text = "Total Pension Insurance Base:";
            // 
            // totalSocialSecurityBaseLabel
            // 
            totalSocialSecurityBaseLabel.AutoSize = true;
            totalSocialSecurityBaseLabel.Location = new System.Drawing.Point(6, 223);
            totalSocialSecurityBaseLabel.Name = "totalSocialSecurityBaseLabel";
            totalSocialSecurityBaseLabel.Size = new System.Drawing.Size(134, 13);
            totalSocialSecurityBaseLabel.TabIndex = 18;
            totalSocialSecurityBaseLabel.Text = "Total Social Security Base:";
            // 
            // totalTaxableLabel
            // 
            totalTaxableLabel.AutoSize = true;
            totalTaxableLabel.Location = new System.Drawing.Point(6, 246);
            totalTaxableLabel.Name = "totalTaxableLabel";
            totalTaxableLabel.Size = new System.Drawing.Size(75, 13);
            totalTaxableLabel.TabIndex = 20;
            totalTaxableLabel.Text = "Total Taxable:";
            // 
            // totalUnemploymentInsuranceBaseLabel
            // 
            totalUnemploymentInsuranceBaseLabel.AutoSize = true;
            totalUnemploymentInsuranceBaseLabel.Location = new System.Drawing.Point(6, 269);
            totalUnemploymentInsuranceBaseLabel.Name = "totalUnemploymentInsuranceBaseLabel";
            totalUnemploymentInsuranceBaseLabel.Size = new System.Drawing.Size(184, 13);
            totalUnemploymentInsuranceBaseLabel.TabIndex = 22;
            totalUnemploymentInsuranceBaseLabel.Text = "Total Unemployment Insurance Base:";
            // 
            // unemploymentLabel
            // 
            unemploymentLabel.AutoSize = true;
            unemploymentLabel.Location = new System.Drawing.Point(6, 292);
            unemploymentLabel.Name = "unemploymentLabel";
            unemploymentLabel.Size = new System.Drawing.Size(80, 13);
            unemploymentLabel.TabIndex = 24;
            unemploymentLabel.Text = "Unemployment:";
            // 
            // allSideCostsLabel
            // 
            allSideCostsLabel.AutoSize = true;
            allSideCostsLabel.Location = new System.Drawing.Point(6, 16);
            allSideCostsLabel.Name = "allSideCostsLabel";
            allSideCostsLabel.Size = new System.Drawing.Size(74, 13);
            allSideCostsLabel.TabIndex = 0;
            allSideCostsLabel.Text = "All Side Costs:";
            // 
            // deductionForeclosureLabel
            // 
            deductionForeclosureLabel.AutoSize = true;
            deductionForeclosureLabel.Location = new System.Drawing.Point(6, 39);
            deductionForeclosureLabel.Name = "deductionForeclosureLabel";
            deductionForeclosureLabel.Size = new System.Drawing.Size(117, 13);
            deductionForeclosureLabel.TabIndex = 2;
            deductionForeclosureLabel.Text = "Deduction Foreclosure:";
            // 
            // deductionOtherDeductionsLabel
            // 
            deductionOtherDeductionsLabel.AutoSize = true;
            deductionOtherDeductionsLabel.Location = new System.Drawing.Point(6, 62);
            deductionOtherDeductionsLabel.Name = "deductionOtherDeductionsLabel";
            deductionOtherDeductionsLabel.Size = new System.Drawing.Size(145, 13);
            deductionOtherDeductionsLabel.TabIndex = 4;
            deductionOtherDeductionsLabel.Text = "Deduction Other Deductions:";
            // 
            // deductionPensionSelfPaymentLabel
            // 
            deductionPensionSelfPaymentLabel.AutoSize = true;
            deductionPensionSelfPaymentLabel.Location = new System.Drawing.Point(6, 85);
            deductionPensionSelfPaymentLabel.Name = "deductionPensionSelfPaymentLabel";
            deductionPensionSelfPaymentLabel.Size = new System.Drawing.Size(165, 13);
            deductionPensionSelfPaymentLabel.TabIndex = 6;
            deductionPensionSelfPaymentLabel.Text = "Deduction Pension Self Payment:";
            // 
            // deductionSalaryAdvanceLabel
            // 
            deductionSalaryAdvanceLabel.AutoSize = true;
            deductionSalaryAdvanceLabel.Location = new System.Drawing.Point(6, 108);
            deductionSalaryAdvanceLabel.Name = "deductionSalaryAdvanceLabel";
            deductionSalaryAdvanceLabel.Size = new System.Drawing.Size(137, 13);
            deductionSalaryAdvanceLabel.TabIndex = 8;
            deductionSalaryAdvanceLabel.Text = "Deduction Salary Advance:";
            // 
            // deductionTaxAndSocialSecuritySelfPaymentLabel
            // 
            deductionTaxAndSocialSecuritySelfPaymentLabel.AutoSize = true;
            deductionTaxAndSocialSecuritySelfPaymentLabel.Location = new System.Drawing.Point(6, 131);
            deductionTaxAndSocialSecuritySelfPaymentLabel.Name = "deductionTaxAndSocialSecuritySelfPaymentLabel";
            deductionTaxAndSocialSecuritySelfPaymentLabel.Size = new System.Drawing.Size(186, 13);
            deductionTaxAndSocialSecuritySelfPaymentLabel.TabIndex = 10;
            deductionTaxAndSocialSecuritySelfPaymentLabel.Text = "Deduction Tax & Soc. Sec. Self Paym.:";
            deductionTaxAndSocialSecuritySelfPaymentLabel.Click += new System.EventHandler(this.deductionTaxAndSocialSecuritySelfPaymentLabel_Click);
            // 
            // deductionUnemploymentSelfPaymentLabel
            // 
            deductionUnemploymentSelfPaymentLabel.AutoSize = true;
            deductionUnemploymentSelfPaymentLabel.Location = new System.Drawing.Point(6, 154);
            deductionUnemploymentSelfPaymentLabel.Name = "deductionUnemploymentSelfPaymentLabel";
            deductionUnemploymentSelfPaymentLabel.Size = new System.Drawing.Size(197, 13);
            deductionUnemploymentSelfPaymentLabel.TabIndex = 12;
            deductionUnemploymentSelfPaymentLabel.Text = "Deduction Unemployment Self Payment:";
            // 
            // deductionUnionPaymentLabel
            // 
            deductionUnionPaymentLabel.AutoSize = true;
            deductionUnionPaymentLabel.Location = new System.Drawing.Point(6, 177);
            deductionUnionPaymentLabel.Name = "deductionUnionPaymentLabel";
            deductionUnionPaymentLabel.Size = new System.Drawing.Size(134, 13);
            deductionUnionPaymentLabel.TabIndex = 14;
            deductionUnionPaymentLabel.Text = "Deduction Union Payment:";
            // 
            // deductionWorkerSelfPaymentLabel
            // 
            deductionWorkerSelfPaymentLabel.AutoSize = true;
            deductionWorkerSelfPaymentLabel.Location = new System.Drawing.Point(6, 200);
            deductionWorkerSelfPaymentLabel.Name = "deductionWorkerSelfPaymentLabel";
            deductionWorkerSelfPaymentLabel.Size = new System.Drawing.Size(162, 13);
            deductionWorkerSelfPaymentLabel.TabIndex = 16;
            deductionWorkerSelfPaymentLabel.Text = "Deduction Worker Self Payment:";
            // 
            // finalCostLabel
            // 
            finalCostLabel.AutoSize = true;
            finalCostLabel.Location = new System.Drawing.Point(6, 223);
            finalCostLabel.Name = "finalCostLabel";
            finalCostLabel.Size = new System.Drawing.Size(56, 13);
            finalCostLabel.TabIndex = 18;
            finalCostLabel.Text = "Final Cost:";
            // 
            // foreclosureByPalkkausLabel
            // 
            foreclosureByPalkkausLabel.AutoSize = true;
            foreclosureByPalkkausLabel.Location = new System.Drawing.Point(6, 246);
            foreclosureByPalkkausLabel.Name = "foreclosureByPalkkausLabel";
            foreclosureByPalkkausLabel.Size = new System.Drawing.Size(127, 13);
            foreclosureByPalkkausLabel.TabIndex = 20;
            foreclosureByPalkkausLabel.Text = "Foreclosure By Palkkaus:";
            // 
            // householdDeductionLabel
            // 
            householdDeductionLabel.AutoSize = true;
            householdDeductionLabel.Location = new System.Drawing.Point(6, 269);
            householdDeductionLabel.Name = "householdDeductionLabel";
            householdDeductionLabel.Size = new System.Drawing.Size(113, 13);
            householdDeductionLabel.TabIndex = 22;
            householdDeductionLabel.Text = "Household Deduction:";
            // 
            // mandatorySideCostsLabel
            // 
            mandatorySideCostsLabel.AutoSize = true;
            mandatorySideCostsLabel.Location = new System.Drawing.Point(6, 292);
            mandatorySideCostsLabel.Name = "mandatorySideCostsLabel";
            mandatorySideCostsLabel.Size = new System.Drawing.Size(113, 13);
            mandatorySideCostsLabel.TabIndex = 24;
            mandatorySideCostsLabel.Text = "Mandatory Side Costs:";
            // 
            // palkkausLabel
            // 
            palkkausLabel.AutoSize = true;
            palkkausLabel.Location = new System.Drawing.Point(6, 315);
            palkkausLabel.Name = "palkkausLabel";
            palkkausLabel.Size = new System.Drawing.Size(54, 13);
            palkkausLabel.TabIndex = 26;
            palkkausLabel.Text = "Palkkaus:";
            // 
            // pensionLabel2
            // 
            pensionLabel2.AutoSize = true;
            pensionLabel2.Location = new System.Drawing.Point(6, 338);
            pensionLabel2.Name = "pensionLabel2";
            pensionLabel2.Size = new System.Drawing.Size(48, 13);
            pensionLabel2.TabIndex = 28;
            pensionLabel2.Text = "Pension:";
            // 
            // socialSecurityLabel
            // 
            socialSecurityLabel.AutoSize = true;
            socialSecurityLabel.Location = new System.Drawing.Point(6, 361);
            socialSecurityLabel.Name = "socialSecurityLabel";
            socialSecurityLabel.Size = new System.Drawing.Size(80, 13);
            socialSecurityLabel.TabIndex = 30;
            socialSecurityLabel.Text = "Social Security:";
            // 
            // totalDeductionsLabel
            // 
            totalDeductionsLabel.AutoSize = true;
            totalDeductionsLabel.Location = new System.Drawing.Point(6, 384);
            totalDeductionsLabel.Name = "totalDeductionsLabel";
            totalDeductionsLabel.Size = new System.Drawing.Size(91, 13);
            totalDeductionsLabel.TabIndex = 32;
            totalDeductionsLabel.Text = "Total Deductions:";
            // 
            // totalPaymentLabel
            // 
            totalPaymentLabel.AutoSize = true;
            totalPaymentLabel.Location = new System.Drawing.Point(6, 407);
            totalPaymentLabel.Name = "totalPaymentLabel";
            totalPaymentLabel.Size = new System.Drawing.Size(78, 13);
            totalPaymentLabel.TabIndex = 34;
            totalPaymentLabel.Text = "Total Payment:";
            // 
            // totalPaymentLegacyLabel
            // 
            totalPaymentLegacyLabel.AutoSize = true;
            totalPaymentLegacyLabel.Location = new System.Drawing.Point(6, 430);
            totalPaymentLegacyLabel.Name = "totalPaymentLegacyLabel";
            totalPaymentLegacyLabel.Size = new System.Drawing.Size(116, 13);
            totalPaymentLegacyLabel.TabIndex = 36;
            totalPaymentLegacyLabel.Text = "Total Payment Legacy:";
            // 
            // totalSalaryCostLabel
            // 
            totalSalaryCostLabel.AutoSize = true;
            totalSalaryCostLabel.Location = new System.Drawing.Point(6, 453);
            totalSalaryCostLabel.Name = "totalSalaryCostLabel";
            totalSalaryCostLabel.Size = new System.Drawing.Size(90, 13);
            totalSalaryCostLabel.TabIndex = 38;
            totalSalaryCostLabel.Text = "Total Salary Cost:";
            // 
            // unemploymentLabel2
            // 
            unemploymentLabel2.AutoSize = true;
            unemploymentLabel2.Location = new System.Drawing.Point(6, 476);
            unemploymentLabel2.Name = "unemploymentLabel2";
            unemploymentLabel2.Size = new System.Drawing.Size(80, 13);
            unemploymentLabel2.TabIndex = 40;
            unemploymentLabel2.Text = "Unemployment:";
            // 
            // benefitsLabel
            // 
            benefitsLabel.AutoSize = true;
            benefitsLabel.Location = new System.Drawing.Point(6, 16);
            benefitsLabel.Name = "benefitsLabel";
            benefitsLabel.Size = new System.Drawing.Size(48, 13);
            benefitsLabel.TabIndex = 0;
            benefitsLabel.Text = "Benefits:";
            // 
            // deductionsLabel
            // 
            deductionsLabel.AutoSize = true;
            deductionsLabel.Location = new System.Drawing.Point(6, 39);
            deductionsLabel.Name = "deductionsLabel";
            deductionsLabel.Size = new System.Drawing.Size(64, 13);
            deductionsLabel.TabIndex = 2;
            deductionsLabel.Text = "Deductions:";
            // 
            // foreclosureLabel
            // 
            foreclosureLabel.AutoSize = true;
            foreclosureLabel.Location = new System.Drawing.Point(6, 62);
            foreclosureLabel.Name = "foreclosureLabel";
            foreclosureLabel.Size = new System.Drawing.Size(65, 13);
            foreclosureLabel.TabIndex = 4;
            foreclosureLabel.Text = "Foreclosure:";
            // 
            // fullOtherDeductionsLabel
            // 
            fullOtherDeductionsLabel.AutoSize = true;
            fullOtherDeductionsLabel.Location = new System.Drawing.Point(6, 85);
            fullOtherDeductionsLabel.Name = "fullOtherDeductionsLabel";
            fullOtherDeductionsLabel.Size = new System.Drawing.Size(112, 13);
            fullOtherDeductionsLabel.TabIndex = 6;
            fullOtherDeductionsLabel.Text = "Full Other Deductions:";
            // 
            // fullPensionLabel
            // 
            fullPensionLabel.AutoSize = true;
            fullPensionLabel.Location = new System.Drawing.Point(6, 108);
            fullPensionLabel.Name = "fullPensionLabel";
            fullPensionLabel.Size = new System.Drawing.Size(67, 13);
            fullPensionLabel.TabIndex = 8;
            fullPensionLabel.Text = "Full Pension:";
            // 
            // fullPrepaidExpensesLabel
            // 
            fullPrepaidExpensesLabel.AutoSize = true;
            fullPrepaidExpensesLabel.Location = new System.Drawing.Point(6, 131);
            fullPrepaidExpensesLabel.Name = "fullPrepaidExpensesLabel";
            fullPrepaidExpensesLabel.Size = new System.Drawing.Size(114, 13);
            fullPrepaidExpensesLabel.TabIndex = 10;
            fullPrepaidExpensesLabel.Text = "Full Prepaid Expenses:";
            // 
            // fullSalaryAdvanceLabel
            // 
            fullSalaryAdvanceLabel.AutoSize = true;
            fullSalaryAdvanceLabel.Location = new System.Drawing.Point(6, 154);
            fullSalaryAdvanceLabel.Name = "fullSalaryAdvanceLabel";
            fullSalaryAdvanceLabel.Size = new System.Drawing.Size(104, 13);
            fullSalaryAdvanceLabel.TabIndex = 12;
            fullSalaryAdvanceLabel.Text = "Full Salary Advance:";
            // 
            // fullTaxLabel
            // 
            fullTaxLabel.AutoSize = true;
            fullTaxLabel.Location = new System.Drawing.Point(6, 177);
            fullTaxLabel.Name = "fullTaxLabel";
            fullTaxLabel.Size = new System.Drawing.Size(47, 13);
            fullTaxLabel.TabIndex = 14;
            fullTaxLabel.Text = "Full Tax:";
            // 
            // fullUnemploymentInsuranceLabel
            // 
            fullUnemploymentInsuranceLabel.AutoSize = true;
            fullUnemploymentInsuranceLabel.Location = new System.Drawing.Point(6, 200);
            fullUnemploymentInsuranceLabel.Name = "fullUnemploymentInsuranceLabel";
            fullUnemploymentInsuranceLabel.Size = new System.Drawing.Size(149, 13);
            fullUnemploymentInsuranceLabel.TabIndex = 16;
            fullUnemploymentInsuranceLabel.Text = "Full Unemployment Insurance:";
            // 
            // fullUnionPaymentLabel
            // 
            fullUnionPaymentLabel.AutoSize = true;
            fullUnionPaymentLabel.Location = new System.Drawing.Point(6, 223);
            fullUnionPaymentLabel.Name = "fullUnionPaymentLabel";
            fullUnionPaymentLabel.Size = new System.Drawing.Size(101, 13);
            fullUnionPaymentLabel.TabIndex = 18;
            fullUnionPaymentLabel.Text = "Full Union Payment:";
            // 
            // otherDeductionsLabel
            // 
            otherDeductionsLabel.AutoSize = true;
            otherDeductionsLabel.Location = new System.Drawing.Point(6, 246);
            otherDeductionsLabel.Name = "otherDeductionsLabel";
            otherDeductionsLabel.Size = new System.Drawing.Size(93, 13);
            otherDeductionsLabel.TabIndex = 20;
            otherDeductionsLabel.Text = "Other Deductions:";
            // 
            // pensionLabel4
            // 
            pensionLabel4.AutoSize = true;
            pensionLabel4.Location = new System.Drawing.Point(6, 269);
            pensionLabel4.Name = "pensionLabel4";
            pensionLabel4.Size = new System.Drawing.Size(48, 13);
            pensionLabel4.TabIndex = 22;
            pensionLabel4.Text = "Pension:";
            // 
            // prepaidExpensesLabel
            // 
            prepaidExpensesLabel.AutoSize = true;
            prepaidExpensesLabel.Location = new System.Drawing.Point(6, 292);
            prepaidExpensesLabel.Name = "prepaidExpensesLabel";
            prepaidExpensesLabel.Size = new System.Drawing.Size(95, 13);
            prepaidExpensesLabel.TabIndex = 24;
            prepaidExpensesLabel.Text = "Prepaid Expenses:";
            // 
            // salaryAdvanceLabel
            // 
            salaryAdvanceLabel.AutoSize = true;
            salaryAdvanceLabel.Location = new System.Drawing.Point(6, 315);
            salaryAdvanceLabel.Name = "salaryAdvanceLabel";
            salaryAdvanceLabel.Size = new System.Drawing.Size(85, 13);
            salaryAdvanceLabel.TabIndex = 26;
            salaryAdvanceLabel.Text = "Salary Advance:";
            // 
            // salaryAfterTaxLabel
            // 
            salaryAfterTaxLabel.AutoSize = true;
            salaryAfterTaxLabel.Location = new System.Drawing.Point(6, 338);
            salaryAfterTaxLabel.Name = "salaryAfterTaxLabel";
            salaryAfterTaxLabel.Size = new System.Drawing.Size(85, 13);
            salaryAfterTaxLabel.TabIndex = 28;
            salaryAfterTaxLabel.Text = "Salary After Tax:";
            // 
            // salaryAfterTaxAndForeclosureLabel
            // 
            salaryAfterTaxAndForeclosureLabel.AutoSize = true;
            salaryAfterTaxAndForeclosureLabel.Location = new System.Drawing.Point(6, 361);
            salaryAfterTaxAndForeclosureLabel.Name = "salaryAfterTaxAndForeclosureLabel";
            salaryAfterTaxAndForeclosureLabel.Size = new System.Drawing.Size(165, 13);
            salaryAfterTaxAndForeclosureLabel.TabIndex = 30;
            salaryAfterTaxAndForeclosureLabel.Text = "Salary After Tax And Foreclosure:";
            // 
            // salaryPaymentLabel
            // 
            salaryPaymentLabel.AutoSize = true;
            salaryPaymentLabel.Location = new System.Drawing.Point(6, 384);
            salaryPaymentLabel.Name = "salaryPaymentLabel";
            salaryPaymentLabel.Size = new System.Drawing.Size(83, 13);
            salaryPaymentLabel.TabIndex = 32;
            salaryPaymentLabel.Text = "Salary Payment:";
            // 
            // taxLabel
            // 
            taxLabel.AutoSize = true;
            taxLabel.Location = new System.Drawing.Point(6, 407);
            taxLabel.Name = "taxLabel";
            taxLabel.Size = new System.Drawing.Size(28, 13);
            taxLabel.TabIndex = 34;
            taxLabel.Text = "Tax:";
            // 
            // totalWorkerPaymentLabel
            // 
            totalWorkerPaymentLabel.AutoSize = true;
            totalWorkerPaymentLabel.Location = new System.Drawing.Point(6, 430);
            totalWorkerPaymentLabel.Name = "totalWorkerPaymentLabel";
            totalWorkerPaymentLabel.Size = new System.Drawing.Size(116, 13);
            totalWorkerPaymentLabel.TabIndex = 36;
            totalWorkerPaymentLabel.Text = "Total Worker Payment:";
            // 
            // totalWorkerPaymentLegacyLabel
            // 
            totalWorkerPaymentLegacyLabel.AutoSize = true;
            totalWorkerPaymentLegacyLabel.Location = new System.Drawing.Point(6, 453);
            totalWorkerPaymentLegacyLabel.Name = "totalWorkerPaymentLegacyLabel";
            totalWorkerPaymentLegacyLabel.Size = new System.Drawing.Size(154, 13);
            totalWorkerPaymentLegacyLabel.TabIndex = 38;
            totalWorkerPaymentLegacyLabel.Text = "Total Worker Payment Legacy:";
            // 
            // unemploymentInsuranceLabel
            // 
            unemploymentInsuranceLabel.AutoSize = true;
            unemploymentInsuranceLabel.Location = new System.Drawing.Point(6, 476);
            unemploymentInsuranceLabel.Name = "unemploymentInsuranceLabel";
            unemploymentInsuranceLabel.Size = new System.Drawing.Size(130, 13);
            unemploymentInsuranceLabel.TabIndex = 40;
            unemploymentInsuranceLabel.Text = "Unemployment Insurance:";
            // 
            // unionPaymentLabel
            // 
            unionPaymentLabel.AutoSize = true;
            unionPaymentLabel.Location = new System.Drawing.Point(6, 499);
            unionPaymentLabel.Name = "unionPaymentLabel";
            unionPaymentLabel.Size = new System.Drawing.Size(82, 13);
            unionPaymentLabel.TabIndex = 42;
            unionPaymentLabel.Text = "Union Payment:";
            // 
            // workerSideCostsLabel
            // 
            workerSideCostsLabel.AutoSize = true;
            workerSideCostsLabel.Location = new System.Drawing.Point(6, 522);
            workerSideCostsLabel.Name = "workerSideCostsLabel";
            workerSideCostsLabel.Size = new System.Drawing.Size(98, 13);
            workerSideCostsLabel.TabIndex = 44;
            workerSideCostsLabel.Text = "Worker Side Costs:";
            // 
            // tabsControl
            // 
            this.tabsControl.Controls.Add(this.tabOverview);
            this.tabsControl.Controls.Add(this.tabInfo);
            this.tabsControl.Controls.Add(this.tabResults);
            this.tabsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabsControl.Location = new System.Drawing.Point(0, 0);
            this.tabsControl.Name = "tabsControl";
            this.tabsControl.SelectedIndex = 0;
            this.tabsControl.Size = new System.Drawing.Size(849, 441);
            this.tabsControl.TabIndex = 0;
            // 
            // tabOverview
            // 
            this.tabOverview.AutoScroll = true;
            this.tabOverview.Controls.Add(this.btnAddRow);
            this.tabOverview.Controls.Add(this.btnSave);
            this.tabOverview.Controls.Add(this.btnRecalculate);
            this.tabOverview.Controls.Add(workEndDateLabel1);
            this.tabOverview.Controls.Add(this.workEndDateDateTimePicker);
            this.tabOverview.Controls.Add(workStartDateLabel1);
            this.tabOverview.Controls.Add(this.workStartDateDateTimePicker);
            this.tabOverview.Controls.Add(createdAtLabel);
            this.tabOverview.Controls.Add(this.createdAtLabel1);
            this.tabOverview.Controls.Add(this.gridRows);
            this.tabOverview.Controls.Add(idLabel2);
            this.tabOverview.Controls.Add(this.idLabel3);
            this.tabOverview.Controls.Add(this.groupBox1);
            this.tabOverview.Location = new System.Drawing.Point(4, 22);
            this.tabOverview.Name = "tabOverview";
            this.tabOverview.Padding = new System.Windows.Forms.Padding(3);
            this.tabOverview.Size = new System.Drawing.Size(841, 415);
            this.tabOverview.TabIndex = 0;
            this.tabOverview.Text = "Overview";
            this.tabOverview.UseVisualStyleBackColor = true;
            // 
            // btnAddRow
            // 
            this.btnAddRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddRow.Location = new System.Drawing.Point(6, 387);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Size = new System.Drawing.Size(93, 23);
            this.btnAddRow.TabIndex = 16;
            this.btnAddRow.Text = "&Add row";
            this.btnAddRow.UseVisualStyleBackColor = true;
            this.btnAddRow.Click += new System.EventHandler(this.btnAddRow_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(742, 388);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(93, 21);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRecalculate
            // 
            this.btnRecalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRecalculate.Location = new System.Drawing.Point(643, 387);
            this.btnRecalculate.Name = "btnRecalculate";
            this.btnRecalculate.Size = new System.Drawing.Size(93, 23);
            this.btnRecalculate.TabIndex = 14;
            this.btnRecalculate.Text = "&Recalculate";
            this.btnRecalculate.UseVisualStyleBackColor = true;
            this.btnRecalculate.Click += new System.EventHandler(this.btnRecalculate_Click);
            // 
            // workEndDateDateTimePicker
            // 
            this.workEndDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.WorkEndDate", true));
            this.workEndDateDateTimePicker.Location = new System.Drawing.Point(394, 30);
            this.workEndDateDateTimePicker.Name = "workEndDateDateTimePicker";
            this.workEndDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.workEndDateDateTimePicker.TabIndex = 9;
            // 
            // workStartDateDateTimePicker
            // 
            this.workStartDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.WorkStartDate", true));
            this.workStartDateDateTimePicker.Location = new System.Drawing.Point(394, 8);
            this.workStartDateDateTimePicker.Name = "workStartDateDateTimePicker";
            this.workStartDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.workStartDateDateTimePicker.TabIndex = 7;
            // 
            // createdAtLabel1
            // 
            this.createdAtLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "CreatedAt", true));
            this.createdAtLabel1.Location = new System.Drawing.Point(122, 34);
            this.createdAtLabel1.Name = "createdAtLabel1";
            this.createdAtLabel1.Size = new System.Drawing.Size(176, 23);
            this.createdAtLabel1.TabIndex = 5;
            this.createdAtLabel1.Text = "label1";
            // 
            // gridRows
            // 
            this.gridRows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridRows.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRows.Location = new System.Drawing.Point(6, 233);
            this.gridRows.Name = "gridRows";
            this.gridRows.Size = new System.Drawing.Size(829, 151);
            this.gridRows.TabIndex = 4;
            // 
            // idLabel3
            // 
            this.idLabel3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Id", true));
            this.idLabel3.Location = new System.Drawing.Point(122, 12);
            this.idLabel3.Name = "idLabel3";
            this.idLabel3.Size = new System.Drawing.Size(176, 23);
            this.idLabel3.TabIndex = 3;
            this.idLabel3.Text = "label1";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(costCenterLabel1);
            this.groupBox1.Controls.Add(this.costCenterTextBox1);
            this.groupBox1.Controls.Add(occupationCodeLabel1);
            this.groupBox1.Controls.Add(this.occupationCodeTextBox1);
            this.groupBox1.Controls.Add(idLabel);
            this.groupBox1.Controls.Add(employmentIdLabel);
            this.groupBox1.Controls.Add(this.idLabel1);
            this.groupBox1.Controls.Add(this.employmentIdLabel1);
            this.groupBox1.Controls.Add(isReadOnlyLabel);
            this.groupBox1.Controls.Add(this.isReadOnlyLabel1);
            this.groupBox1.Controls.Add(displayNameLabel);
            this.groupBox1.Controls.Add(this.displayNameLabel1);
            this.groupBox1.Controls.Add(accountIdLabel);
            this.groupBox1.Controls.Add(this.accountIdLabel1);
            this.groupBox1.Controls.Add(emailLabel);
            this.groupBox1.Controls.Add(this.emailLabel1);
            this.groupBox1.Controls.Add(ibanNumberLabel);
            this.groupBox1.Controls.Add(this.ibanNumberLabel1);
            this.groupBox1.Controls.Add(socialSecurityNumberValidLabel);
            this.groupBox1.Controls.Add(this.socialSecurityNumberValidLabel1);
            this.groupBox1.Controls.Add(telephoneLabel);
            this.groupBox1.Controls.Add(this.telephoneLabel1);
            this.groupBox1.Location = new System.Drawing.Point(6, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(829, 141);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Worker / Employment relation";
            // 
            // costCenterTextBox1
            // 
            this.costCenterTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.CostCenter", true));
            this.costCenterTextBox1.Location = new System.Drawing.Point(388, 112);
            this.costCenterTextBox1.Name = "costCenterTextBox1";
            this.costCenterTextBox1.Size = new System.Drawing.Size(100, 20);
            this.costCenterTextBox1.TabIndex = 25;
            // 
            // occupationCodeTextBox1
            // 
            this.occupationCodeTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.OccupationCode", true));
            this.occupationCodeTextBox1.Location = new System.Drawing.Point(388, 89);
            this.occupationCodeTextBox1.Name = "occupationCodeTextBox1";
            this.occupationCodeTextBox1.Size = new System.Drawing.Size(100, 20);
            this.occupationCodeTextBox1.TabIndex = 24;
            // 
            // idLabel1
            // 
            this.idLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Id", true));
            this.idLabel1.Location = new System.Drawing.Point(33, -26);
            this.idLabel1.Name = "idLabel1";
            this.idLabel1.Size = new System.Drawing.Size(100, 23);
            this.idLabel1.TabIndex = 3;
            this.idLabel1.Text = "label1";
            // 
            // employmentIdLabel1
            // 
            this.employmentIdLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.EmploymentId", true));
            this.employmentIdLabel1.Location = new System.Drawing.Point(113, 46);
            this.employmentIdLabel1.Name = "employmentIdLabel1";
            this.employmentIdLabel1.Size = new System.Drawing.Size(170, 23);
            this.employmentIdLabel1.TabIndex = 23;
            this.employmentIdLabel1.Text = "label1";
            // 
            // isReadOnlyLabel1
            // 
            this.isReadOnlyLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.Avatar.IsReadOnly", true));
            this.isReadOnlyLabel1.Location = new System.Drawing.Point(113, 92);
            this.isReadOnlyLabel1.Name = "isReadOnlyLabel1";
            this.isReadOnlyLabel1.Size = new System.Drawing.Size(170, 23);
            this.isReadOnlyLabel1.TabIndex = 19;
            this.isReadOnlyLabel1.Text = "label1";
            // 
            // displayNameLabel1
            // 
            this.displayNameLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.Avatar.DisplayName", true));
            this.displayNameLabel1.Location = new System.Drawing.Point(113, 69);
            this.displayNameLabel1.Name = "displayNameLabel1";
            this.displayNameLabel1.Size = new System.Drawing.Size(170, 23);
            this.displayNameLabel1.TabIndex = 17;
            this.displayNameLabel1.Text = "label1";
            // 
            // accountIdLabel1
            // 
            this.accountIdLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.AccountId", true));
            this.accountIdLabel1.Location = new System.Drawing.Point(113, 23);
            this.accountIdLabel1.Name = "accountIdLabel1";
            this.accountIdLabel1.Size = new System.Drawing.Size(170, 23);
            this.accountIdLabel1.TabIndex = 15;
            this.accountIdLabel1.Text = "label1";
            // 
            // emailLabel1
            // 
            this.emailLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.PaymentData.Email", true));
            this.emailLabel1.Location = new System.Drawing.Point(385, 23);
            this.emailLabel1.Name = "emailLabel1";
            this.emailLabel1.Size = new System.Drawing.Size(242, 23);
            this.emailLabel1.TabIndex = 1;
            this.emailLabel1.Text = "label1";
            // 
            // ibanNumberLabel1
            // 
            this.ibanNumberLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.PaymentData.IbanNumber", true));
            this.ibanNumberLabel1.Location = new System.Drawing.Point(385, 46);
            this.ibanNumberLabel1.Name = "ibanNumberLabel1";
            this.ibanNumberLabel1.Size = new System.Drawing.Size(242, 23);
            this.ibanNumberLabel1.TabIndex = 5;
            this.ibanNumberLabel1.Text = "label1";
            // 
            // socialSecurityNumberValidLabel1
            // 
            this.socialSecurityNumberValidLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.PaymentData.SocialSecurityNumberValid", true));
            this.socialSecurityNumberValidLabel1.Location = new System.Drawing.Point(113, 115);
            this.socialSecurityNumberValidLabel1.Name = "socialSecurityNumberValidLabel1";
            this.socialSecurityNumberValidLabel1.Size = new System.Drawing.Size(163, 23);
            this.socialSecurityNumberValidLabel1.TabIndex = 11;
            this.socialSecurityNumberValidLabel1.Text = "label1";
            // 
            // telephoneLabel1
            // 
            this.telephoneLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Worker.PaymentData.Telephone", true));
            this.telephoneLabel1.Location = new System.Drawing.Point(385, 69);
            this.telephoneLabel1.Name = "telephoneLabel1";
            this.telephoneLabel1.Size = new System.Drawing.Size(242, 23);
            this.telephoneLabel1.TabIndex = 13;
            this.telephoneLabel1.Text = "label1";
            // 
            // tabInfo
            // 
            this.tabInfo.AutoScroll = true;
            this.tabInfo.Controls.Add(backofficeNotesLabel);
            this.tabInfo.Controls.Add(this.backofficeNotesTextBox);
            this.tabInfo.Controls.Add(costCenterLabel);
            this.tabInfo.Controls.Add(this.costCenterTextBox);
            this.tabInfo.Controls.Add(occupationCodeLabel);
            this.tabInfo.Controls.Add(this.occupationCodeTextBox);
            this.tabInfo.Controls.Add(paymentIdLabel);
            this.tabInfo.Controls.Add(this.paymentIdTextBox);
            this.tabInfo.Controls.Add(payrollIdLabel);
            this.tabInfo.Controls.Add(this.payrollIdTextBox);
            this.tabInfo.Controls.Add(pensionPaymentDateLabel);
            this.tabInfo.Controls.Add(this.pensionPaymentDateTextBox);
            this.tabInfo.Controls.Add(pensionPaymentRefLabel);
            this.tabInfo.Controls.Add(this.pensionPaymentRefTextBox);
            this.tabInfo.Controls.Add(pensionPaymentSpecifierLabel);
            this.tabInfo.Controls.Add(this.pensionPaymentSpecifierTextBox);
            this.tabInfo.Controls.Add(projectNumberLabel);
            this.tabInfo.Controls.Add(this.projectNumberTextBox);
            this.tabInfo.Controls.Add(salarySlipMessageLabel);
            this.tabInfo.Controls.Add(this.salarySlipMessageTextBox);
            this.tabInfo.Controls.Add(workDescriptionLabel);
            this.tabInfo.Controls.Add(this.workDescriptionTextBox);
            this.tabInfo.Controls.Add(workEndDateLabel);
            this.tabInfo.Controls.Add(this.workEndDateTextBox);
            this.tabInfo.Controls.Add(workerMessageLabel);
            this.tabInfo.Controls.Add(this.workerMessageTextBox);
            this.tabInfo.Controls.Add(workStartDateLabel);
            this.tabInfo.Controls.Add(this.workStartDateTextBox);
            this.tabInfo.Location = new System.Drawing.Point(4, 22);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfo.Size = new System.Drawing.Size(841, 415);
            this.tabInfo.TabIndex = 1;
            this.tabInfo.Text = "Info";
            // 
            // backofficeNotesTextBox
            // 
            this.backofficeNotesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.BackofficeNotes", true));
            this.backofficeNotesTextBox.Location = new System.Drawing.Point(148, 9);
            this.backofficeNotesTextBox.Name = "backofficeNotesTextBox";
            this.backofficeNotesTextBox.Size = new System.Drawing.Size(100, 20);
            this.backofficeNotesTextBox.TabIndex = 1;
            // 
            // costCenterTextBox
            // 
            this.costCenterTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.CostCenter", true));
            this.costCenterTextBox.Location = new System.Drawing.Point(148, 35);
            this.costCenterTextBox.Name = "costCenterTextBox";
            this.costCenterTextBox.Size = new System.Drawing.Size(100, 20);
            this.costCenterTextBox.TabIndex = 3;
            // 
            // occupationCodeTextBox
            // 
            this.occupationCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.OccupationCode", true));
            this.occupationCodeTextBox.Location = new System.Drawing.Point(148, 61);
            this.occupationCodeTextBox.Name = "occupationCodeTextBox";
            this.occupationCodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.occupationCodeTextBox.TabIndex = 5;
            // 
            // paymentIdTextBox
            // 
            this.paymentIdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.PaymentId", true));
            this.paymentIdTextBox.Location = new System.Drawing.Point(148, 87);
            this.paymentIdTextBox.Name = "paymentIdTextBox";
            this.paymentIdTextBox.Size = new System.Drawing.Size(100, 20);
            this.paymentIdTextBox.TabIndex = 7;
            // 
            // payrollIdTextBox
            // 
            this.payrollIdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.PayrollId", true));
            this.payrollIdTextBox.Location = new System.Drawing.Point(148, 113);
            this.payrollIdTextBox.Name = "payrollIdTextBox";
            this.payrollIdTextBox.Size = new System.Drawing.Size(100, 20);
            this.payrollIdTextBox.TabIndex = 9;
            // 
            // pensionPaymentDateTextBox
            // 
            this.pensionPaymentDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.PensionPaymentDate", true));
            this.pensionPaymentDateTextBox.Location = new System.Drawing.Point(148, 139);
            this.pensionPaymentDateTextBox.Name = "pensionPaymentDateTextBox";
            this.pensionPaymentDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.pensionPaymentDateTextBox.TabIndex = 11;
            // 
            // pensionPaymentRefTextBox
            // 
            this.pensionPaymentRefTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.PensionPaymentRef", true));
            this.pensionPaymentRefTextBox.Location = new System.Drawing.Point(148, 165);
            this.pensionPaymentRefTextBox.Name = "pensionPaymentRefTextBox";
            this.pensionPaymentRefTextBox.Size = new System.Drawing.Size(100, 20);
            this.pensionPaymentRefTextBox.TabIndex = 13;
            // 
            // pensionPaymentSpecifierTextBox
            // 
            this.pensionPaymentSpecifierTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.PensionPaymentSpecifier", true));
            this.pensionPaymentSpecifierTextBox.Location = new System.Drawing.Point(148, 191);
            this.pensionPaymentSpecifierTextBox.Name = "pensionPaymentSpecifierTextBox";
            this.pensionPaymentSpecifierTextBox.Size = new System.Drawing.Size(100, 20);
            this.pensionPaymentSpecifierTextBox.TabIndex = 15;
            // 
            // projectNumberTextBox
            // 
            this.projectNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.ProjectNumber", true));
            this.projectNumberTextBox.Location = new System.Drawing.Point(148, 217);
            this.projectNumberTextBox.Name = "projectNumberTextBox";
            this.projectNumberTextBox.Size = new System.Drawing.Size(100, 20);
            this.projectNumberTextBox.TabIndex = 17;
            // 
            // salarySlipMessageTextBox
            // 
            this.salarySlipMessageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.SalarySlipMessage", true));
            this.salarySlipMessageTextBox.Location = new System.Drawing.Point(148, 243);
            this.salarySlipMessageTextBox.Name = "salarySlipMessageTextBox";
            this.salarySlipMessageTextBox.Size = new System.Drawing.Size(100, 20);
            this.salarySlipMessageTextBox.TabIndex = 19;
            // 
            // workDescriptionTextBox
            // 
            this.workDescriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.WorkDescription", true));
            this.workDescriptionTextBox.Location = new System.Drawing.Point(148, 269);
            this.workDescriptionTextBox.Name = "workDescriptionTextBox";
            this.workDescriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.workDescriptionTextBox.TabIndex = 21;
            // 
            // workEndDateTextBox
            // 
            this.workEndDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.WorkEndDate", true));
            this.workEndDateTextBox.Location = new System.Drawing.Point(148, 295);
            this.workEndDateTextBox.Name = "workEndDateTextBox";
            this.workEndDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.workEndDateTextBox.TabIndex = 23;
            // 
            // workerMessageTextBox
            // 
            this.workerMessageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.WorkerMessage", true));
            this.workerMessageTextBox.Location = new System.Drawing.Point(148, 321);
            this.workerMessageTextBox.Name = "workerMessageTextBox";
            this.workerMessageTextBox.Size = new System.Drawing.Size(100, 20);
            this.workerMessageTextBox.TabIndex = 25;
            // 
            // workStartDateTextBox
            // 
            this.workStartDateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Info.WorkStartDate", true));
            this.workStartDateTextBox.Location = new System.Drawing.Point(148, 347);
            this.workStartDateTextBox.Name = "workStartDateTextBox";
            this.workStartDateTextBox.Size = new System.Drawing.Size(100, 20);
            this.workStartDateTextBox.TabIndex = 27;
            // 
            // tabResults
            // 
            this.tabResults.AutoScroll = true;
            this.tabResults.Controls.Add(this.groupBox4);
            this.tabResults.Controls.Add(this.groupBox3);
            this.tabResults.Controls.Add(this.groupBox2);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabResults.Size = new System.Drawing.Size(841, 415);
            this.tabResults.TabIndex = 2;
            this.tabResults.Text = "Results";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(benefitsLabel);
            this.groupBox4.Controls.Add(this.benefitsLabel1);
            this.groupBox4.Controls.Add(deductionsLabel);
            this.groupBox4.Controls.Add(this.deductionsLabel1);
            this.groupBox4.Controls.Add(foreclosureLabel);
            this.groupBox4.Controls.Add(this.foreclosureLabel1);
            this.groupBox4.Controls.Add(fullOtherDeductionsLabel);
            this.groupBox4.Controls.Add(this.fullOtherDeductionsLabel1);
            this.groupBox4.Controls.Add(fullPensionLabel);
            this.groupBox4.Controls.Add(this.fullPensionLabel1);
            this.groupBox4.Controls.Add(fullPrepaidExpensesLabel);
            this.groupBox4.Controls.Add(this.fullPrepaidExpensesLabel1);
            this.groupBox4.Controls.Add(fullSalaryAdvanceLabel);
            this.groupBox4.Controls.Add(this.fullSalaryAdvanceLabel1);
            this.groupBox4.Controls.Add(fullTaxLabel);
            this.groupBox4.Controls.Add(this.fullTaxLabel1);
            this.groupBox4.Controls.Add(fullUnemploymentInsuranceLabel);
            this.groupBox4.Controls.Add(this.fullUnemploymentInsuranceLabel1);
            this.groupBox4.Controls.Add(fullUnionPaymentLabel);
            this.groupBox4.Controls.Add(this.fullUnionPaymentLabel1);
            this.groupBox4.Controls.Add(otherDeductionsLabel);
            this.groupBox4.Controls.Add(this.otherDeductionsLabel1);
            this.groupBox4.Controls.Add(pensionLabel4);
            this.groupBox4.Controls.Add(this.pensionLabel5);
            this.groupBox4.Controls.Add(prepaidExpensesLabel);
            this.groupBox4.Controls.Add(this.prepaidExpensesLabel1);
            this.groupBox4.Controls.Add(salaryAdvanceLabel);
            this.groupBox4.Controls.Add(this.salaryAdvanceLabel1);
            this.groupBox4.Controls.Add(salaryAfterTaxLabel);
            this.groupBox4.Controls.Add(this.salaryAfterTaxLabel1);
            this.groupBox4.Controls.Add(salaryAfterTaxAndForeclosureLabel);
            this.groupBox4.Controls.Add(this.salaryAfterTaxAndForeclosureLabel1);
            this.groupBox4.Controls.Add(salaryPaymentLabel);
            this.groupBox4.Controls.Add(this.salaryPaymentLabel1);
            this.groupBox4.Controls.Add(taxLabel);
            this.groupBox4.Controls.Add(this.taxLabel1);
            this.groupBox4.Controls.Add(totalWorkerPaymentLabel);
            this.groupBox4.Controls.Add(this.totalWorkerPaymentLabel1);
            this.groupBox4.Controls.Add(totalWorkerPaymentLegacyLabel);
            this.groupBox4.Controls.Add(this.totalWorkerPaymentLegacyLabel1);
            this.groupBox4.Controls.Add(unemploymentInsuranceLabel);
            this.groupBox4.Controls.Add(this.unemploymentInsuranceLabel1);
            this.groupBox4.Controls.Add(unionPaymentLabel);
            this.groupBox4.Controls.Add(this.unionPaymentLabel1);
            this.groupBox4.Controls.Add(workerSideCostsLabel);
            this.groupBox4.Controls.Add(this.workerSideCostsLabel1);
            this.groupBox4.Location = new System.Drawing.Point(534, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(284, 550);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Worker";
            // 
            // benefitsLabel1
            // 
            this.benefitsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.Benefits", true));
            this.benefitsLabel1.Location = new System.Drawing.Point(177, 16);
            this.benefitsLabel1.Name = "benefitsLabel1";
            this.benefitsLabel1.Size = new System.Drawing.Size(100, 23);
            this.benefitsLabel1.TabIndex = 1;
            this.benefitsLabel1.Text = "label1";
            // 
            // deductionsLabel1
            // 
            this.deductionsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.Deductions", true));
            this.deductionsLabel1.Location = new System.Drawing.Point(177, 39);
            this.deductionsLabel1.Name = "deductionsLabel1";
            this.deductionsLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionsLabel1.TabIndex = 3;
            this.deductionsLabel1.Text = "label1";
            // 
            // foreclosureLabel1
            // 
            this.foreclosureLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.Foreclosure", true));
            this.foreclosureLabel1.Location = new System.Drawing.Point(177, 62);
            this.foreclosureLabel1.Name = "foreclosureLabel1";
            this.foreclosureLabel1.Size = new System.Drawing.Size(100, 23);
            this.foreclosureLabel1.TabIndex = 5;
            this.foreclosureLabel1.Text = "label1";
            // 
            // fullOtherDeductionsLabel1
            // 
            this.fullOtherDeductionsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullOtherDeductions", true));
            this.fullOtherDeductionsLabel1.Location = new System.Drawing.Point(177, 85);
            this.fullOtherDeductionsLabel1.Name = "fullOtherDeductionsLabel1";
            this.fullOtherDeductionsLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullOtherDeductionsLabel1.TabIndex = 7;
            this.fullOtherDeductionsLabel1.Text = "label1";
            // 
            // fullPensionLabel1
            // 
            this.fullPensionLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullPension", true));
            this.fullPensionLabel1.Location = new System.Drawing.Point(177, 108);
            this.fullPensionLabel1.Name = "fullPensionLabel1";
            this.fullPensionLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullPensionLabel1.TabIndex = 9;
            this.fullPensionLabel1.Text = "label1";
            // 
            // fullPrepaidExpensesLabel1
            // 
            this.fullPrepaidExpensesLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullPrepaidExpenses", true));
            this.fullPrepaidExpensesLabel1.Location = new System.Drawing.Point(177, 131);
            this.fullPrepaidExpensesLabel1.Name = "fullPrepaidExpensesLabel1";
            this.fullPrepaidExpensesLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullPrepaidExpensesLabel1.TabIndex = 11;
            this.fullPrepaidExpensesLabel1.Text = "label1";
            // 
            // fullSalaryAdvanceLabel1
            // 
            this.fullSalaryAdvanceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullSalaryAdvance", true));
            this.fullSalaryAdvanceLabel1.Location = new System.Drawing.Point(177, 154);
            this.fullSalaryAdvanceLabel1.Name = "fullSalaryAdvanceLabel1";
            this.fullSalaryAdvanceLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullSalaryAdvanceLabel1.TabIndex = 13;
            this.fullSalaryAdvanceLabel1.Text = "label1";
            // 
            // fullTaxLabel1
            // 
            this.fullTaxLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullTax", true));
            this.fullTaxLabel1.Location = new System.Drawing.Point(177, 177);
            this.fullTaxLabel1.Name = "fullTaxLabel1";
            this.fullTaxLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullTaxLabel1.TabIndex = 15;
            this.fullTaxLabel1.Text = "label1";
            // 
            // fullUnemploymentInsuranceLabel1
            // 
            this.fullUnemploymentInsuranceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullUnemploymentInsurance", true));
            this.fullUnemploymentInsuranceLabel1.Location = new System.Drawing.Point(177, 200);
            this.fullUnemploymentInsuranceLabel1.Name = "fullUnemploymentInsuranceLabel1";
            this.fullUnemploymentInsuranceLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullUnemploymentInsuranceLabel1.TabIndex = 17;
            this.fullUnemploymentInsuranceLabel1.Text = "label1";
            // 
            // fullUnionPaymentLabel1
            // 
            this.fullUnionPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.FullUnionPayment", true));
            this.fullUnionPaymentLabel1.Location = new System.Drawing.Point(177, 223);
            this.fullUnionPaymentLabel1.Name = "fullUnionPaymentLabel1";
            this.fullUnionPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.fullUnionPaymentLabel1.TabIndex = 19;
            this.fullUnionPaymentLabel1.Text = "label1";
            // 
            // otherDeductionsLabel1
            // 
            this.otherDeductionsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.OtherDeductions", true));
            this.otherDeductionsLabel1.Location = new System.Drawing.Point(177, 246);
            this.otherDeductionsLabel1.Name = "otherDeductionsLabel1";
            this.otherDeductionsLabel1.Size = new System.Drawing.Size(100, 23);
            this.otherDeductionsLabel1.TabIndex = 21;
            this.otherDeductionsLabel1.Text = "label1";
            // 
            // pensionLabel5
            // 
            this.pensionLabel5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.Pension", true));
            this.pensionLabel5.Location = new System.Drawing.Point(177, 269);
            this.pensionLabel5.Name = "pensionLabel5";
            this.pensionLabel5.Size = new System.Drawing.Size(100, 23);
            this.pensionLabel5.TabIndex = 23;
            this.pensionLabel5.Text = "label1";
            // 
            // prepaidExpensesLabel1
            // 
            this.prepaidExpensesLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.PrepaidExpenses", true));
            this.prepaidExpensesLabel1.Location = new System.Drawing.Point(177, 292);
            this.prepaidExpensesLabel1.Name = "prepaidExpensesLabel1";
            this.prepaidExpensesLabel1.Size = new System.Drawing.Size(100, 23);
            this.prepaidExpensesLabel1.TabIndex = 25;
            this.prepaidExpensesLabel1.Text = "label1";
            // 
            // salaryAdvanceLabel1
            // 
            this.salaryAdvanceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.SalaryAdvance", true));
            this.salaryAdvanceLabel1.Location = new System.Drawing.Point(177, 315);
            this.salaryAdvanceLabel1.Name = "salaryAdvanceLabel1";
            this.salaryAdvanceLabel1.Size = new System.Drawing.Size(100, 23);
            this.salaryAdvanceLabel1.TabIndex = 27;
            this.salaryAdvanceLabel1.Text = "label1";
            // 
            // salaryAfterTaxLabel1
            // 
            this.salaryAfterTaxLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.SalaryAfterTax", true));
            this.salaryAfterTaxLabel1.Location = new System.Drawing.Point(177, 338);
            this.salaryAfterTaxLabel1.Name = "salaryAfterTaxLabel1";
            this.salaryAfterTaxLabel1.Size = new System.Drawing.Size(100, 23);
            this.salaryAfterTaxLabel1.TabIndex = 29;
            this.salaryAfterTaxLabel1.Text = "label1";
            // 
            // salaryAfterTaxAndForeclosureLabel1
            // 
            this.salaryAfterTaxAndForeclosureLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.SalaryAfterTaxAndForeclosure", true));
            this.salaryAfterTaxAndForeclosureLabel1.Location = new System.Drawing.Point(177, 361);
            this.salaryAfterTaxAndForeclosureLabel1.Name = "salaryAfterTaxAndForeclosureLabel1";
            this.salaryAfterTaxAndForeclosureLabel1.Size = new System.Drawing.Size(100, 23);
            this.salaryAfterTaxAndForeclosureLabel1.TabIndex = 31;
            this.salaryAfterTaxAndForeclosureLabel1.Text = "label1";
            // 
            // salaryPaymentLabel1
            // 
            this.salaryPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.SalaryPayment", true));
            this.salaryPaymentLabel1.Location = new System.Drawing.Point(177, 384);
            this.salaryPaymentLabel1.Name = "salaryPaymentLabel1";
            this.salaryPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.salaryPaymentLabel1.TabIndex = 33;
            this.salaryPaymentLabel1.Text = "label1";
            // 
            // taxLabel1
            // 
            this.taxLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.Tax", true));
            this.taxLabel1.Location = new System.Drawing.Point(177, 407);
            this.taxLabel1.Name = "taxLabel1";
            this.taxLabel1.Size = new System.Drawing.Size(100, 23);
            this.taxLabel1.TabIndex = 35;
            this.taxLabel1.Text = "label1";
            // 
            // totalWorkerPaymentLabel1
            // 
            this.totalWorkerPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.TotalWorkerPayment", true));
            this.totalWorkerPaymentLabel1.Location = new System.Drawing.Point(177, 430);
            this.totalWorkerPaymentLabel1.Name = "totalWorkerPaymentLabel1";
            this.totalWorkerPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalWorkerPaymentLabel1.TabIndex = 37;
            this.totalWorkerPaymentLabel1.Text = "label1";
            // 
            // totalWorkerPaymentLegacyLabel1
            // 
            this.totalWorkerPaymentLegacyLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.TotalWorkerPaymentLegacy", true));
            this.totalWorkerPaymentLegacyLabel1.Location = new System.Drawing.Point(177, 453);
            this.totalWorkerPaymentLegacyLabel1.Name = "totalWorkerPaymentLegacyLabel1";
            this.totalWorkerPaymentLegacyLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalWorkerPaymentLegacyLabel1.TabIndex = 39;
            this.totalWorkerPaymentLegacyLabel1.Text = "label1";
            // 
            // unemploymentInsuranceLabel1
            // 
            this.unemploymentInsuranceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.UnemploymentInsurance", true));
            this.unemploymentInsuranceLabel1.Location = new System.Drawing.Point(177, 476);
            this.unemploymentInsuranceLabel1.Name = "unemploymentInsuranceLabel1";
            this.unemploymentInsuranceLabel1.Size = new System.Drawing.Size(100, 23);
            this.unemploymentInsuranceLabel1.TabIndex = 41;
            this.unemploymentInsuranceLabel1.Text = "label1";
            // 
            // unionPaymentLabel1
            // 
            this.unionPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.UnionPayment", true));
            this.unionPaymentLabel1.Location = new System.Drawing.Point(177, 499);
            this.unionPaymentLabel1.Name = "unionPaymentLabel1";
            this.unionPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.unionPaymentLabel1.TabIndex = 43;
            this.unionPaymentLabel1.Text = "label1";
            // 
            // workerSideCostsLabel1
            // 
            this.workerSideCostsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.WorkerCalc.WorkerSideCosts", true));
            this.workerSideCostsLabel1.Location = new System.Drawing.Point(177, 522);
            this.workerSideCostsLabel1.Name = "workerSideCostsLabel1";
            this.workerSideCostsLabel1.Size = new System.Drawing.Size(100, 23);
            this.workerSideCostsLabel1.TabIndex = 45;
            this.workerSideCostsLabel1.Text = "label1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(allSideCostsLabel);
            this.groupBox3.Controls.Add(this.allSideCostsLabel1);
            this.groupBox3.Controls.Add(deductionForeclosureLabel);
            this.groupBox3.Controls.Add(this.deductionForeclosureLabel1);
            this.groupBox3.Controls.Add(deductionOtherDeductionsLabel);
            this.groupBox3.Controls.Add(this.deductionOtherDeductionsLabel1);
            this.groupBox3.Controls.Add(deductionPensionSelfPaymentLabel);
            this.groupBox3.Controls.Add(this.deductionPensionSelfPaymentLabel1);
            this.groupBox3.Controls.Add(deductionSalaryAdvanceLabel);
            this.groupBox3.Controls.Add(this.deductionSalaryAdvanceLabel1);
            this.groupBox3.Controls.Add(deductionTaxAndSocialSecuritySelfPaymentLabel);
            this.groupBox3.Controls.Add(this.deductionTaxAndSocialSecuritySelfPaymentLabel1);
            this.groupBox3.Controls.Add(deductionUnemploymentSelfPaymentLabel);
            this.groupBox3.Controls.Add(this.deductionUnemploymentSelfPaymentLabel1);
            this.groupBox3.Controls.Add(deductionUnionPaymentLabel);
            this.groupBox3.Controls.Add(this.deductionUnionPaymentLabel1);
            this.groupBox3.Controls.Add(deductionWorkerSelfPaymentLabel);
            this.groupBox3.Controls.Add(this.deductionWorkerSelfPaymentLabel1);
            this.groupBox3.Controls.Add(finalCostLabel);
            this.groupBox3.Controls.Add(this.finalCostLabel1);
            this.groupBox3.Controls.Add(foreclosureByPalkkausLabel);
            this.groupBox3.Controls.Add(this.foreclosureByPalkkausLabel1);
            this.groupBox3.Controls.Add(householdDeductionLabel);
            this.groupBox3.Controls.Add(this.householdDeductionLabel1);
            this.groupBox3.Controls.Add(mandatorySideCostsLabel);
            this.groupBox3.Controls.Add(this.mandatorySideCostsLabel1);
            this.groupBox3.Controls.Add(palkkausLabel);
            this.groupBox3.Controls.Add(this.palkkausLabel1);
            this.groupBox3.Controls.Add(pensionLabel2);
            this.groupBox3.Controls.Add(this.pensionLabel3);
            this.groupBox3.Controls.Add(socialSecurityLabel);
            this.groupBox3.Controls.Add(this.socialSecurityLabel1);
            this.groupBox3.Controls.Add(totalDeductionsLabel);
            this.groupBox3.Controls.Add(this.totalDeductionsLabel1);
            this.groupBox3.Controls.Add(totalPaymentLabel);
            this.groupBox3.Controls.Add(this.totalPaymentLabel1);
            this.groupBox3.Controls.Add(totalPaymentLegacyLabel);
            this.groupBox3.Controls.Add(this.totalPaymentLegacyLabel1);
            this.groupBox3.Controls.Add(totalSalaryCostLabel);
            this.groupBox3.Controls.Add(this.totalSalaryCostLabel1);
            this.groupBox3.Controls.Add(unemploymentLabel2);
            this.groupBox3.Controls.Add(this.unemploymentLabel3);
            this.groupBox3.Location = new System.Drawing.Point(261, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(267, 504);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Employer";
            // 
            // allSideCostsLabel1
            // 
            this.allSideCostsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.AllSideCosts", true));
            this.allSideCostsLabel1.Location = new System.Drawing.Point(206, 16);
            this.allSideCostsLabel1.Name = "allSideCostsLabel1";
            this.allSideCostsLabel1.Size = new System.Drawing.Size(100, 23);
            this.allSideCostsLabel1.TabIndex = 1;
            this.allSideCostsLabel1.Text = "label1";
            // 
            // deductionForeclosureLabel1
            // 
            this.deductionForeclosureLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionForeclosure", true));
            this.deductionForeclosureLabel1.Location = new System.Drawing.Point(206, 39);
            this.deductionForeclosureLabel1.Name = "deductionForeclosureLabel1";
            this.deductionForeclosureLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionForeclosureLabel1.TabIndex = 3;
            this.deductionForeclosureLabel1.Text = "label1";
            // 
            // deductionOtherDeductionsLabel1
            // 
            this.deductionOtherDeductionsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionOtherDeductions", true));
            this.deductionOtherDeductionsLabel1.Location = new System.Drawing.Point(206, 62);
            this.deductionOtherDeductionsLabel1.Name = "deductionOtherDeductionsLabel1";
            this.deductionOtherDeductionsLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionOtherDeductionsLabel1.TabIndex = 5;
            this.deductionOtherDeductionsLabel1.Text = "label1";
            // 
            // deductionPensionSelfPaymentLabel1
            // 
            this.deductionPensionSelfPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionPensionSelfPayment", true));
            this.deductionPensionSelfPaymentLabel1.Location = new System.Drawing.Point(206, 85);
            this.deductionPensionSelfPaymentLabel1.Name = "deductionPensionSelfPaymentLabel1";
            this.deductionPensionSelfPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionPensionSelfPaymentLabel1.TabIndex = 7;
            this.deductionPensionSelfPaymentLabel1.Text = "label1";
            // 
            // deductionSalaryAdvanceLabel1
            // 
            this.deductionSalaryAdvanceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionSalaryAdvance", true));
            this.deductionSalaryAdvanceLabel1.Location = new System.Drawing.Point(206, 108);
            this.deductionSalaryAdvanceLabel1.Name = "deductionSalaryAdvanceLabel1";
            this.deductionSalaryAdvanceLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionSalaryAdvanceLabel1.TabIndex = 9;
            this.deductionSalaryAdvanceLabel1.Text = "label1";
            // 
            // deductionTaxAndSocialSecuritySelfPaymentLabel1
            // 
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionTaxAndSocialSecuritySelfPayment", true));
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1.Location = new System.Drawing.Point(206, 131);
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1.Name = "deductionTaxAndSocialSecuritySelfPaymentLabel1";
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1.TabIndex = 11;
            this.deductionTaxAndSocialSecuritySelfPaymentLabel1.Text = "label1";
            // 
            // deductionUnemploymentSelfPaymentLabel1
            // 
            this.deductionUnemploymentSelfPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionUnemploymentSelfPayment", true));
            this.deductionUnemploymentSelfPaymentLabel1.Location = new System.Drawing.Point(206, 154);
            this.deductionUnemploymentSelfPaymentLabel1.Name = "deductionUnemploymentSelfPaymentLabel1";
            this.deductionUnemploymentSelfPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionUnemploymentSelfPaymentLabel1.TabIndex = 13;
            this.deductionUnemploymentSelfPaymentLabel1.Text = "label1";
            // 
            // deductionUnionPaymentLabel1
            // 
            this.deductionUnionPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionUnionPayment", true));
            this.deductionUnionPaymentLabel1.Location = new System.Drawing.Point(206, 177);
            this.deductionUnionPaymentLabel1.Name = "deductionUnionPaymentLabel1";
            this.deductionUnionPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionUnionPaymentLabel1.TabIndex = 15;
            this.deductionUnionPaymentLabel1.Text = "label1";
            // 
            // deductionWorkerSelfPaymentLabel1
            // 
            this.deductionWorkerSelfPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.DeductionWorkerSelfPayment", true));
            this.deductionWorkerSelfPaymentLabel1.Location = new System.Drawing.Point(206, 200);
            this.deductionWorkerSelfPaymentLabel1.Name = "deductionWorkerSelfPaymentLabel1";
            this.deductionWorkerSelfPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.deductionWorkerSelfPaymentLabel1.TabIndex = 17;
            this.deductionWorkerSelfPaymentLabel1.Text = "label1";
            // 
            // finalCostLabel1
            // 
            this.finalCostLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.FinalCost", true));
            this.finalCostLabel1.Location = new System.Drawing.Point(206, 223);
            this.finalCostLabel1.Name = "finalCostLabel1";
            this.finalCostLabel1.Size = new System.Drawing.Size(100, 23);
            this.finalCostLabel1.TabIndex = 19;
            this.finalCostLabel1.Text = "label1";
            // 
            // foreclosureByPalkkausLabel1
            // 
            this.foreclosureByPalkkausLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.ForeclosureByPalkkaus", true));
            this.foreclosureByPalkkausLabel1.Location = new System.Drawing.Point(206, 246);
            this.foreclosureByPalkkausLabel1.Name = "foreclosureByPalkkausLabel1";
            this.foreclosureByPalkkausLabel1.Size = new System.Drawing.Size(100, 23);
            this.foreclosureByPalkkausLabel1.TabIndex = 21;
            this.foreclosureByPalkkausLabel1.Text = "label1";
            // 
            // householdDeductionLabel1
            // 
            this.householdDeductionLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.HouseholdDeduction", true));
            this.householdDeductionLabel1.Location = new System.Drawing.Point(206, 269);
            this.householdDeductionLabel1.Name = "householdDeductionLabel1";
            this.householdDeductionLabel1.Size = new System.Drawing.Size(100, 23);
            this.householdDeductionLabel1.TabIndex = 23;
            this.householdDeductionLabel1.Text = "label1";
            // 
            // mandatorySideCostsLabel1
            // 
            this.mandatorySideCostsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.MandatorySideCosts", true));
            this.mandatorySideCostsLabel1.Location = new System.Drawing.Point(206, 292);
            this.mandatorySideCostsLabel1.Name = "mandatorySideCostsLabel1";
            this.mandatorySideCostsLabel1.Size = new System.Drawing.Size(100, 23);
            this.mandatorySideCostsLabel1.TabIndex = 25;
            this.mandatorySideCostsLabel1.Text = "label1";
            // 
            // palkkausLabel1
            // 
            this.palkkausLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.Palkkaus", true));
            this.palkkausLabel1.Location = new System.Drawing.Point(206, 315);
            this.palkkausLabel1.Name = "palkkausLabel1";
            this.palkkausLabel1.Size = new System.Drawing.Size(100, 23);
            this.palkkausLabel1.TabIndex = 27;
            this.palkkausLabel1.Text = "label1";
            // 
            // pensionLabel3
            // 
            this.pensionLabel3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.Pension", true));
            this.pensionLabel3.Location = new System.Drawing.Point(206, 338);
            this.pensionLabel3.Name = "pensionLabel3";
            this.pensionLabel3.Size = new System.Drawing.Size(100, 23);
            this.pensionLabel3.TabIndex = 29;
            this.pensionLabel3.Text = "label1";
            // 
            // socialSecurityLabel1
            // 
            this.socialSecurityLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.SocialSecurity", true));
            this.socialSecurityLabel1.Location = new System.Drawing.Point(206, 361);
            this.socialSecurityLabel1.Name = "socialSecurityLabel1";
            this.socialSecurityLabel1.Size = new System.Drawing.Size(100, 23);
            this.socialSecurityLabel1.TabIndex = 31;
            this.socialSecurityLabel1.Text = "label1";
            // 
            // totalDeductionsLabel1
            // 
            this.totalDeductionsLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.TotalDeductions", true));
            this.totalDeductionsLabel1.Location = new System.Drawing.Point(206, 384);
            this.totalDeductionsLabel1.Name = "totalDeductionsLabel1";
            this.totalDeductionsLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalDeductionsLabel1.TabIndex = 33;
            this.totalDeductionsLabel1.Text = "label1";
            // 
            // totalPaymentLabel1
            // 
            this.totalPaymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.TotalPayment", true));
            this.totalPaymentLabel1.Location = new System.Drawing.Point(206, 407);
            this.totalPaymentLabel1.Name = "totalPaymentLabel1";
            this.totalPaymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalPaymentLabel1.TabIndex = 35;
            this.totalPaymentLabel1.Text = "label1";
            // 
            // totalPaymentLegacyLabel1
            // 
            this.totalPaymentLegacyLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.TotalPaymentLegacy", true));
            this.totalPaymentLegacyLabel1.Location = new System.Drawing.Point(206, 430);
            this.totalPaymentLegacyLabel1.Name = "totalPaymentLegacyLabel1";
            this.totalPaymentLegacyLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalPaymentLegacyLabel1.TabIndex = 37;
            this.totalPaymentLegacyLabel1.Text = "label1";
            // 
            // totalSalaryCostLabel1
            // 
            this.totalSalaryCostLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.TotalSalaryCost", true));
            this.totalSalaryCostLabel1.Location = new System.Drawing.Point(206, 453);
            this.totalSalaryCostLabel1.Name = "totalSalaryCostLabel1";
            this.totalSalaryCostLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalSalaryCostLabel1.TabIndex = 39;
            this.totalSalaryCostLabel1.Text = "label1";
            // 
            // unemploymentLabel3
            // 
            this.unemploymentLabel3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.EmployerCalc.Unemployment", true));
            this.unemploymentLabel3.Location = new System.Drawing.Point(206, 476);
            this.unemploymentLabel3.Name = "unemploymentLabel3";
            this.unemploymentLabel3.Size = new System.Drawing.Size(100, 23);
            this.unemploymentLabel3.TabIndex = 41;
            this.unemploymentLabel3.Text = "label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(pensionLabel);
            this.groupBox2.Controls.Add(this.pensionLabel1);
            this.groupBox2.Controls.Add(totalLabel);
            this.groupBox2.Controls.Add(this.totalLabel1);
            this.groupBox2.Controls.Add(totalAccidentInsuranceBaseLabel);
            this.groupBox2.Controls.Add(this.totalAccidentInsuranceBaseLabel1);
            this.groupBox2.Controls.Add(totalBaseSalaryLabel);
            this.groupBox2.Controls.Add(this.totalBaseSalaryLabel1);
            this.groupBox2.Controls.Add(totalExpensesLabel);
            this.groupBox2.Controls.Add(this.totalExpensesLabel1);
            this.groupBox2.Controls.Add(totalGrossSalaryLabel);
            this.groupBox2.Controls.Add(this.totalGrossSalaryLabel1);
            this.groupBox2.Controls.Add(totalHealthInsuranceBaseLabel);
            this.groupBox2.Controls.Add(this.totalHealthInsuranceBaseLabel1);
            this.groupBox2.Controls.Add(totalPayableLabel);
            this.groupBox2.Controls.Add(this.totalPayableLabel1);
            this.groupBox2.Controls.Add(totalPensionInsuranceBaseLabel);
            this.groupBox2.Controls.Add(this.totalPensionInsuranceBaseLabel1);
            this.groupBox2.Controls.Add(totalSocialSecurityBaseLabel);
            this.groupBox2.Controls.Add(this.totalSocialSecurityBaseLabel1);
            this.groupBox2.Controls.Add(totalTaxableLabel);
            this.groupBox2.Controls.Add(this.totalTaxableLabel1);
            this.groupBox2.Controls.Add(totalUnemploymentInsuranceBaseLabel);
            this.groupBox2.Controls.Add(this.totalUnemploymentInsuranceBaseLabel1);
            this.groupBox2.Controls.Add(unemploymentLabel);
            this.groupBox2.Controls.Add(this.unemploymentLabel1);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 329);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Totals";
            // 
            // pensionLabel1
            // 
            this.pensionLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.Pension", true));
            this.pensionLabel1.Location = new System.Drawing.Point(196, 16);
            this.pensionLabel1.Name = "pensionLabel1";
            this.pensionLabel1.Size = new System.Drawing.Size(100, 23);
            this.pensionLabel1.TabIndex = 1;
            this.pensionLabel1.Text = "label1";
            // 
            // totalLabel1
            // 
            this.totalLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.Total", true));
            this.totalLabel1.Location = new System.Drawing.Point(196, 39);
            this.totalLabel1.Name = "totalLabel1";
            this.totalLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalLabel1.TabIndex = 3;
            this.totalLabel1.Text = "label1";
            // 
            // totalAccidentInsuranceBaseLabel1
            // 
            this.totalAccidentInsuranceBaseLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalAccidentInsuranceBase", true));
            this.totalAccidentInsuranceBaseLabel1.Location = new System.Drawing.Point(196, 62);
            this.totalAccidentInsuranceBaseLabel1.Name = "totalAccidentInsuranceBaseLabel1";
            this.totalAccidentInsuranceBaseLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalAccidentInsuranceBaseLabel1.TabIndex = 5;
            this.totalAccidentInsuranceBaseLabel1.Text = "label1";
            // 
            // totalBaseSalaryLabel1
            // 
            this.totalBaseSalaryLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalBaseSalary", true));
            this.totalBaseSalaryLabel1.Location = new System.Drawing.Point(196, 85);
            this.totalBaseSalaryLabel1.Name = "totalBaseSalaryLabel1";
            this.totalBaseSalaryLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalBaseSalaryLabel1.TabIndex = 7;
            this.totalBaseSalaryLabel1.Text = "label1";
            // 
            // totalExpensesLabel1
            // 
            this.totalExpensesLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalExpenses", true));
            this.totalExpensesLabel1.Location = new System.Drawing.Point(196, 108);
            this.totalExpensesLabel1.Name = "totalExpensesLabel1";
            this.totalExpensesLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalExpensesLabel1.TabIndex = 9;
            this.totalExpensesLabel1.Text = "label1";
            // 
            // totalGrossSalaryLabel1
            // 
            this.totalGrossSalaryLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalGrossSalary", true));
            this.totalGrossSalaryLabel1.Location = new System.Drawing.Point(196, 131);
            this.totalGrossSalaryLabel1.Name = "totalGrossSalaryLabel1";
            this.totalGrossSalaryLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalGrossSalaryLabel1.TabIndex = 11;
            this.totalGrossSalaryLabel1.Text = "label1";
            // 
            // totalHealthInsuranceBaseLabel1
            // 
            this.totalHealthInsuranceBaseLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalHealthInsuranceBase", true));
            this.totalHealthInsuranceBaseLabel1.Location = new System.Drawing.Point(196, 154);
            this.totalHealthInsuranceBaseLabel1.Name = "totalHealthInsuranceBaseLabel1";
            this.totalHealthInsuranceBaseLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalHealthInsuranceBaseLabel1.TabIndex = 13;
            this.totalHealthInsuranceBaseLabel1.Text = "label1";
            // 
            // totalPayableLabel1
            // 
            this.totalPayableLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalPayable", true));
            this.totalPayableLabel1.Location = new System.Drawing.Point(196, 177);
            this.totalPayableLabel1.Name = "totalPayableLabel1";
            this.totalPayableLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalPayableLabel1.TabIndex = 15;
            this.totalPayableLabel1.Text = "label1";
            // 
            // totalPensionInsuranceBaseLabel1
            // 
            this.totalPensionInsuranceBaseLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalPensionInsuranceBase", true));
            this.totalPensionInsuranceBaseLabel1.Location = new System.Drawing.Point(196, 200);
            this.totalPensionInsuranceBaseLabel1.Name = "totalPensionInsuranceBaseLabel1";
            this.totalPensionInsuranceBaseLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalPensionInsuranceBaseLabel1.TabIndex = 17;
            this.totalPensionInsuranceBaseLabel1.Text = "label1";
            // 
            // totalSocialSecurityBaseLabel1
            // 
            this.totalSocialSecurityBaseLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalSocialSecurityBase", true));
            this.totalSocialSecurityBaseLabel1.Location = new System.Drawing.Point(196, 223);
            this.totalSocialSecurityBaseLabel1.Name = "totalSocialSecurityBaseLabel1";
            this.totalSocialSecurityBaseLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalSocialSecurityBaseLabel1.TabIndex = 19;
            this.totalSocialSecurityBaseLabel1.Text = "label1";
            // 
            // totalTaxableLabel1
            // 
            this.totalTaxableLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalTaxable", true));
            this.totalTaxableLabel1.Location = new System.Drawing.Point(196, 246);
            this.totalTaxableLabel1.Name = "totalTaxableLabel1";
            this.totalTaxableLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalTaxableLabel1.TabIndex = 21;
            this.totalTaxableLabel1.Text = "label1";
            // 
            // totalUnemploymentInsuranceBaseLabel1
            // 
            this.totalUnemploymentInsuranceBaseLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.TotalUnemploymentInsuranceBase", true));
            this.totalUnemploymentInsuranceBaseLabel1.Location = new System.Drawing.Point(196, 269);
            this.totalUnemploymentInsuranceBaseLabel1.Name = "totalUnemploymentInsuranceBaseLabel1";
            this.totalUnemploymentInsuranceBaseLabel1.Size = new System.Drawing.Size(100, 23);
            this.totalUnemploymentInsuranceBaseLabel1.TabIndex = 23;
            this.totalUnemploymentInsuranceBaseLabel1.Text = "label1";
            // 
            // unemploymentLabel1
            // 
            this.unemploymentLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.calculationBindingSource, "Result.Totals.Unemployment", true));
            this.unemploymentLabel1.Location = new System.Drawing.Point(196, 292);
            this.unemploymentLabel1.Name = "unemploymentLabel1";
            this.unemploymentLabel1.Size = new System.Drawing.Size(100, 23);
            this.unemploymentLabel1.TabIndex = 25;
            this.unemploymentLabel1.Text = "label1";
            // 
            // calculationBindingSource
            // 
            this.calculationBindingSource.DataSource = typeof(Salaxy.Calculation);
            this.calculationBindingSource.CurrentChanged += new System.EventHandler(this.calculationBindingSource_CurrentChanged);
            // 
            // CalculatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabsControl);
            this.Name = "CalculatorControl";
            this.Size = new System.Drawing.Size(849, 441);
            this.tabsControl.ResumeLayout(false);
            this.tabOverview.ResumeLayout(false);
            this.tabOverview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRows)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabInfo.ResumeLayout(false);
            this.tabInfo.PerformLayout();
            this.tabResults.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calculationBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabsControl;
        private System.Windows.Forms.TabPage tabOverview;
        private System.Windows.Forms.TabPage tabInfo;
        private System.Windows.Forms.BindingSource calculationBindingSource;
        private System.Windows.Forms.TextBox backofficeNotesTextBox;
        private System.Windows.Forms.TextBox costCenterTextBox;
        private System.Windows.Forms.TextBox occupationCodeTextBox;
        private System.Windows.Forms.TextBox paymentIdTextBox;
        private System.Windows.Forms.TextBox payrollIdTextBox;
        private System.Windows.Forms.TextBox pensionPaymentDateTextBox;
        private System.Windows.Forms.TextBox pensionPaymentRefTextBox;
        private System.Windows.Forms.TextBox pensionPaymentSpecifierTextBox;
        private System.Windows.Forms.TextBox projectNumberTextBox;
        private System.Windows.Forms.TextBox salarySlipMessageTextBox;
        private System.Windows.Forms.TextBox workDescriptionTextBox;
        private System.Windows.Forms.TextBox workEndDateTextBox;
        private System.Windows.Forms.TextBox workerMessageTextBox;
        private System.Windows.Forms.TextBox workStartDateTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label employmentIdLabel1;
        private System.Windows.Forms.Label isReadOnlyLabel1;
        private System.Windows.Forms.Label displayNameLabel1;
        private System.Windows.Forms.Label accountIdLabel1;
        private System.Windows.Forms.Label emailLabel1;
        private System.Windows.Forms.Label ibanNumberLabel1;
        private System.Windows.Forms.Label socialSecurityNumberValidLabel1;
        private System.Windows.Forms.Label telephoneLabel1;
        private System.Windows.Forms.Label idLabel3;
        private System.Windows.Forms.Label idLabel1;
        private System.Windows.Forms.DataGridView gridRows;
        private System.Windows.Forms.DateTimePicker workEndDateDateTimePicker;
        private System.Windows.Forms.DateTimePicker workStartDateDateTimePicker;
        private System.Windows.Forms.Label createdAtLabel1;
        private System.Windows.Forms.TextBox costCenterTextBox1;
        private System.Windows.Forms.TextBox occupationCodeTextBox1;
        private System.Windows.Forms.Button btnRecalculate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.TabPage tabResults;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label allSideCostsLabel1;
        private System.Windows.Forms.Label deductionForeclosureLabel1;
        private System.Windows.Forms.Label deductionOtherDeductionsLabel1;
        private System.Windows.Forms.Label deductionPensionSelfPaymentLabel1;
        private System.Windows.Forms.Label deductionSalaryAdvanceLabel1;
        private System.Windows.Forms.Label deductionTaxAndSocialSecuritySelfPaymentLabel1;
        private System.Windows.Forms.Label deductionUnemploymentSelfPaymentLabel1;
        private System.Windows.Forms.Label deductionUnionPaymentLabel1;
        private System.Windows.Forms.Label deductionWorkerSelfPaymentLabel1;
        private System.Windows.Forms.Label finalCostLabel1;
        private System.Windows.Forms.Label foreclosureByPalkkausLabel1;
        private System.Windows.Forms.Label householdDeductionLabel1;
        private System.Windows.Forms.Label mandatorySideCostsLabel1;
        private System.Windows.Forms.Label palkkausLabel1;
        private System.Windows.Forms.Label pensionLabel3;
        private System.Windows.Forms.Label socialSecurityLabel1;
        private System.Windows.Forms.Label totalDeductionsLabel1;
        private System.Windows.Forms.Label totalPaymentLabel1;
        private System.Windows.Forms.Label totalPaymentLegacyLabel1;
        private System.Windows.Forms.Label totalSalaryCostLabel1;
        private System.Windows.Forms.Label unemploymentLabel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label pensionLabel1;
        private System.Windows.Forms.Label totalLabel1;
        private System.Windows.Forms.Label totalAccidentInsuranceBaseLabel1;
        private System.Windows.Forms.Label totalBaseSalaryLabel1;
        private System.Windows.Forms.Label totalExpensesLabel1;
        private System.Windows.Forms.Label totalGrossSalaryLabel1;
        private System.Windows.Forms.Label totalHealthInsuranceBaseLabel1;
        private System.Windows.Forms.Label totalPayableLabel1;
        private System.Windows.Forms.Label totalPensionInsuranceBaseLabel1;
        private System.Windows.Forms.Label totalSocialSecurityBaseLabel1;
        private System.Windows.Forms.Label totalTaxableLabel1;
        private System.Windows.Forms.Label totalUnemploymentInsuranceBaseLabel1;
        private System.Windows.Forms.Label unemploymentLabel1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label benefitsLabel1;
        private System.Windows.Forms.Label deductionsLabel1;
        private System.Windows.Forms.Label foreclosureLabel1;
        private System.Windows.Forms.Label fullOtherDeductionsLabel1;
        private System.Windows.Forms.Label fullPensionLabel1;
        private System.Windows.Forms.Label fullPrepaidExpensesLabel1;
        private System.Windows.Forms.Label fullSalaryAdvanceLabel1;
        private System.Windows.Forms.Label fullTaxLabel1;
        private System.Windows.Forms.Label fullUnemploymentInsuranceLabel1;
        private System.Windows.Forms.Label fullUnionPaymentLabel1;
        private System.Windows.Forms.Label otherDeductionsLabel1;
        private System.Windows.Forms.Label pensionLabel5;
        private System.Windows.Forms.Label prepaidExpensesLabel1;
        private System.Windows.Forms.Label salaryAdvanceLabel1;
        private System.Windows.Forms.Label salaryAfterTaxLabel1;
        private System.Windows.Forms.Label salaryAfterTaxAndForeclosureLabel1;
        private System.Windows.Forms.Label salaryPaymentLabel1;
        private System.Windows.Forms.Label taxLabel1;
        private System.Windows.Forms.Label totalWorkerPaymentLabel1;
        private System.Windows.Forms.Label totalWorkerPaymentLegacyLabel1;
        private System.Windows.Forms.Label unemploymentInsuranceLabel1;
        private System.Windows.Forms.Label unionPaymentLabel1;
        private System.Windows.Forms.Label workerSideCostsLabel1;
    }
}
