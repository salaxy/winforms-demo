﻿
namespace SalaxyDemo.Forms
{
    partial class FrmPayroll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.payrollControl = new SalaxyDemo.Forms.PayrollControl();
            this.SuspendLayout();
            // 
            // payrollControl
            // 
            this.payrollControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.payrollControl.Location = new System.Drawing.Point(0, 0);
            this.payrollControl.Name = "payrollControl";
            this.payrollControl.Size = new System.Drawing.Size(800, 450);
            this.payrollControl.TabIndex = 0;
            // 
            // FrmPayroll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.payrollControl);
            this.Name = "FrmPayroll";
            this.Text = "FrmPayroll";
            this.Load += new System.EventHandler(this.FrmPayroll_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private PayrollControl payrollControl;
    }
}