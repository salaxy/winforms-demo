﻿using Salaxy;
using SalaxyDemo.Forms;
using SalaxyDemo.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalaxyDemo.Forms
{
    /// <summary>
    /// Form that shows API list (OData) in a grid.
    /// </summary>
    public partial class FrmGrid : Form
    {
        public FrmGrid()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Type of the list item that is shown in the grid.
        /// </summary>
        ApiListItemType Type { get; set; }

        /// <summary>
        /// Http helper with authentication.
        /// </summary>
        SalaxyApiHelper Api { get; set; }

        /// <summary>
        /// Server-side order-by clause
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        /// Server-side filter clause
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Static method for opening the MDI form.
        /// </summary>
        /// <param name="parent">PArent MDI form</param>
        /// <param name="type">Type of list item to open</param>
        /// <returns>The new form that was opened.</returns>
        public static async Task<FrmGrid> OpenMdi(FrmMain parent, ApiListItemType type)
        {
            var newForm = new FrmGrid();
            newForm.MdiParent = parent;
            newForm.Type = type;
            newForm.Api = parent.Api;
            newForm.Text = type.ToString() + " list";
            newForm.setData(await parent.Api.Search(type));
            newForm.Show();
            return newForm;
        }

        /// <summary>
        /// Opens an item in a details window (Property grid by default).
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task OpenItem(string id)
        {
            var item = await Api.GetItem(this.Type, id);
            switch (Type)
            {
                case ApiListItemType.Calculation:
                case ApiListItemType.CalculationPaid:
                    {
                        var newForm = new FrmCalculator();
                        newForm.SetCalculation(item, Api);
                        newForm.MdiParent = this.MdiParent;
                        newForm.Show();
                        break;
                    }
                case ApiListItemType.PayrollDetails:
                    {
                        var newForm = new FrmPayroll();
                        newForm.SetPayroll(item, Api);
                        newForm.MdiParent = this.MdiParent;
                        newForm.Show();
                        break;
                    }
                default:
                    {
                        var newForm = new FrmPropertyGrid();
                        var proxy = new Model.AutoUi.Proxy(item);
                        newForm.PropertyGrid.SelectedObject = proxy;
                        newForm.MdiParent = this.MdiParent;
                        newForm.Text = $"{this.Type}: {id}";
                        newForm.Show();
                        break;
                    }
            }
        }

        /// <summary>
        /// Sets the data to the given API list item array.
        /// </summary>
        /// <param name="data">The data</param>
        private void setData(System.Collections.IEnumerable data)
        {
            // TODO: This is a bit naive implementation, but in real-life we would probably use a better Data Grid component in any case.
            var statuses = new List<string>();
            statuses.Add("None");
            foreach (dynamic item in data)
            {
                var status = (item.Status ?? "").ToString();
                if (status != "" && !statuses.Contains(status))
                {
                    statuses.Add(status);
                }
            }
            cbxFilter.Items.Clear();
            cbxFilter.Items.AddRange(statuses.ToArray());

            dataGridView1.DataSource = data;
            dataGridView1.Columns["Id"].DisplayIndex = 0;

            dataGridView1.Columns["Status"].DisplayIndex = 1;
            dataGridView1.Columns["ShortText"].DisplayIndex = 2;

            dataGridView1.Columns["CreatedAt"].DisplayIndex = 3;
            dataGridView1.Columns["UpdatedAt"].DisplayIndex = 4;
            dataGridView1.Columns["SalaryDate"].DisplayIndex = 5;
            dataGridView1.Columns["LogicalDate"].DisplayIndex = 6;

            // TODO: There is som problem in sorting columns like this => set index again. Would probably need to create the columns, but the result is good enough for demo purposes.
            dataGridView1.Columns["Status"].DisplayIndex = 1;
            dataGridView1.Columns["ShortText"].DisplayIndex = 2;

            // TODO: Add other party
            dataGridView1.Columns["OtherId"].DisplayIndex = 7;
            dataGridView1.Columns["OtherPartyInfo"].DisplayIndex = 8;

            // Data would be relevant, position probably here, but needs functionality to add (include and exclude in the function)
            dataGridView1.Columns["Data"].Visible = false;

            dataGridView1.Columns["GrossSalary"].DisplayIndex = 10;
            dataGridView1.Columns["Payment"].DisplayIndex = 11;
            dataGridView1.Columns["Fee"].DisplayIndex = 12;

            dataGridView1.Columns["EntityType"].DisplayIndex = 13;
            dataGridView1.Columns["Reference"].DisplayIndex = 14;
            dataGridView1.Columns["IsReadOnly"].DisplayIndex = 15;
            dataGridView1.Columns["VersionNumber"].DisplayIndex = 16;
            // Not sure if these will be shown in a sensible way
            dataGridView1.Columns["BusinessObjects"].DisplayIndex = 17;
            dataGridView1.Columns["Flags"].DisplayIndex = 18;
            dataGridView1.Columns["Messages"].DisplayIndex = 19;

            // Hide at least by default
            dataGridView1.Columns["BackOfficeStatus"].Visible = false;
            dataGridView1.Columns["StartAt"].Visible = false;
            dataGridView1.Columns["EndAt"].Visible = false;
            dataGridView1.Columns["Uri"].Visible = false;
            dataGridView1.Columns["BackOfficeStatus"].Visible = false;

            // Set these visible if showing data from multiple owners
            dataGridView1.Columns["OwnerId"].Visible = false;
            dataGridView1.Columns["OwnerInfo"].Visible = false;
            dataGridView1.Columns["Owner"].Visible = false;
            dataGridView1.Columns["Partner"].Visible = false;
        }

        private void frmGrid_Load(object sender, EventArgs e)
        {
            
        }

        private async void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dynamic item = this.dataGridView1.Rows[e.RowIndex].DataBoundItem;
            if (item != null)
            {
                if (Type == ApiListItemType.WorkerAccount || Type == ApiListItemType.Employment)
                {
                    // NOTE: In the current API version, this is now an exception in the API
                    await OpenItem(item.OtherId);
                }
                else
                {
                    await OpenItem(item.Id);
                }
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView grid = (DataGridView)sender;
            DataGridViewRow row = grid.Rows[e.RowIndex];
            DataGridViewColumn col = grid.Columns[e.ColumnIndex];
            if (row.DataBoundItem != null)
            {
                switch (col.DataPropertyName)
                {
                    case "OtherPartyInfo":
                    case "OwnerInfo":
                        {
                            var obj = e.Value as AccountInIndex;
                            if (obj != null)
                            {
                                e.Value = obj.Avatar.SortableName;
                            }
                            break;
                        }
                    case "CreatedAt":
                    case "UpdatedAt":
                    case "SalaryDate":
                    case "LogicalDate":
                        {
                            var obj = e.Value as DateTimeOffset?;
                            if (obj != null)
                            {
                                e.Value = obj.Value.ToString("dd.MM.yyyy"); // HACK: Need to handle timezone if going to production
                            }
                            break;
                        }
                }
            }
            /*
            // Add this for property path support (https://stackoverflow.com/questions/121274/is-it-possible-to-bind-complex-type-properties-to-a-datagrid)
            if (row.DataBoundItem != null && col.DataPropertyName.Contains("."))
            {
                string[] props = col.DataPropertyName.Split('.');
                PropertyInfo propInfo = row.DataBoundItem.GetType().GetProperty(props[0]);
                object val = propInfo.GetValue(row.DataBoundItem, null);
                for (int i = 1; i < props.Length; i++)
                {
                    propInfo = val.GetType().GetProperty(props[i]);
                    val = propInfo.GetValue(val, null);
                }
                e.Value = val;
            }
            */
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            setData(await Api.Search(Type, txtSearch.Text, Filter, OrderBy));
        }

        private async void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var orderBy = dataGridView1.Columns[e.ColumnIndex].DataPropertyName;
            // Need to convert the .Net property name to its JSON equivalent (as in Open API). E.g. "CreatedAt" => "createdAt".
            orderBy = orderBy.Substring(0, 1).ToLower() + orderBy.Substring(1);
            if (OrderBy == orderBy)
            {
                // Syntax is "[field name] [asc/desc]", where "asc" is default (can be omitted).
                // Not all fields support ordering. Most notably, all fields under data property (the type specific data) only support filtering, not sorting.
                orderBy = orderBy + " desc";
            }
            try
            {
                setData(await Api.Search(Type, txtSearch.Text, Filter, orderBy));
                OrderBy = orderBy;
            }
            catch (Salaxy.ApiException)
            {
                MessageBox.Show($"ERROR: Unable to sort with '{orderBy}'. This is probably because not all columns in lists are sortable.");
            }
        }

        private async void btnFilter_Click(object sender, EventArgs e)
        {
            var filter = cbxFilter.SelectedItem as string;
            // Need to convert the .Net enum name to its JSON equivalent (as in Open API). E.g. "CreatedAt" => "createdAt".
            filter = filter.Substring(0, 1).ToLower() + filter.Substring(1);
            if ((filter ?? "none").ToLower() == "none")
            {
                filter = null;
            }
            else
            {
                // See e.g. "4.5. Filter System Query Option ($filter)" in https://www.odata.org/documentation/odata-version-2-0/uri-conventions/
                // Generally, the supported filter operators are eq (=), lt (<), le (<=), gt (>), ge (>=) and ne (!=).
                // ...also groping with parenthesis, "and", "or" and "not".
                // Most, but not all fields support filtering.
                filter = $"status eq '{filter}'";
            }
            Filter = filter;
            setData(await Api.Search(Type, txtSearch.Text, Filter, OrderBy));
        }
    }
}
