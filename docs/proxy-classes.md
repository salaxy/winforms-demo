API Proxy classes
=================

The proxy classes can simply be generated using the out-of-the-box functionality of Visual Studio. That is also how we created the in “\winforms-demo\OpenAPIs” folder:

In Visual Studio, right-click “References” and select “Manage Connected Services”  
![Manage references screen shot](./pics/proxy-classes/manage-references.png)
 
In the “Connected Services” dialog, click “Add a Service reference”  
![Add a service reference screen shot](./pics/proxy-classes/add-a-service-reference.png)
 
Select ”Open API” in the “Add Service reference” window and click “Next”  
![Select Open API screen shot](./pics/proxy-classes/select-open-api.png)
 
Generate the API from https://developers.salaxy.com/docs/swagger/salaxy-02.json  
![Generate screen shot](./pics/proxy-classes/finnish.png)
 
Depending on your project, you may need to add `System.ComponentModel.DataAnnotations` or other references to your project before the generated code works.

If you need more control on the code generation, we highly recommend NSwagStudio in https://github.com/RicoSuter/NSwag/wiki/NSwagStudio or just NSwag directly. NSwag is what the wizard above uses internally, but Visual Studio uses an older version and there is not so much generation options.