Payroll import process
======================

Create a new Payroll
--------------------

There is an example of how to create a new payroll in [FrmMain.cs](../Forms/FrmMain.cs):

```c#
private async void mnuNewPayroll_Click(object sender, EventArgs e)
{
    // Some defaults - these would of course depend on the case.
    var start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
    PayrollDetails newItem = new PayrollDetails() {
        Input = new PayrollInput
        {
            NoUpdateFromEmployment = true, // Meaning: When adding calculations, do not take default rows from the employment relation.
            Period = new DateRange()
            {
                Start = start,
                End = start.AddMonths(1).AddDays(-1),
                DaysCount = 15,
            },
            SalaryDate = start.AddMonths(1).AddDays(4),
            WageBasis = WageBasis.Hourly,
            Title = "Demo payroll",
        },
        Info = new Payroll03Info(),
    };
    var newForm = new FrmPayroll();
    newForm.SetPayroll(newItem, Api);
    newForm.MdiParent = this;
    newForm.Show();
}
```
 
You may then save the Payroll as shown in [SalaxyApiHelper.cs](../Model/SalaxyApiHelper.cs) (SaveItem method):
```c#
case ApiListItemType.PayrollDetails:
    {
        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
        var result = await api.ODataSearchAsync(search, null, filter, orderby, null);
        return result.Items.Cast<dynamic>().ToList();
    }
```

Note that since there is no calculations yet in the Payroll, you will get a validation error in the response message.
This is perfectly OK: the validation error simply tells you that you cannot pay this Payroll yet,
obviously as there is no calculation to pay. However the item is saved and available in the UI
if the response code is 200.
 
Once saved, you can simply store the ID in your own database and fetch the Payroll on the next save:
```c#
case ApiListItemType.PayrollDetails:
    {
        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
        return await api.CrudGetAsync(id);
    }
```
 
If you need to later search for a payroll, you can search for them using the OData API:
```c#
case ApiListItemType.PayrollDetails:
    {
        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
        var result = await api.ODataSearchAsync(search, null, filter, orderby, null);
        return result.Items.Cast<dynamic>().ToList();
    }
```
 
E.g. if you wish to check whether a payroll already exists for a given salary date, you could set the filter as “salaryDate eq ‘2022-07-01’” (equal) or `salaryDate ge '2022-07-01'` (greater or equal). Note that the field names in these queries are `lowerCamelCase` as in Open API / JSON, not `UpperCamelCase` as in .Net. Also note that the result here is list items, not the full payload: You need to load the payload separately (see above).

Add calculations to Payroll
---------------------------

The demo shows how to add calculations to Payroll for a set of Personal IDs (Finnish HeTu). The personal IDs must already exist in Palkkaus.fi (there will be another example on how to create them using API). You can find the example in [SomeCustomImportService.cs](../Model//ImportDemo/SomeCustomImportRow.cs) (ImportRows):

```c#
/// <summary>
/// Does a demo import to an existing Payroll object
/// </summary>
/// <param name="payroll">Payroll to update</param>
/// <param name="rows">Rows to import</param>
/// <param name="api">Authenticated Salaxy API helper instance</param>
/// <returns>Stored Payroll if the import was successfull, null if it was not.</returns>
public async Task<PayrollDetails> ImportRows(PayrollDetails payroll, List<SomeCustomImportRow> rows, SalaxyApiHelper api)
{
    if (payroll.Id == null)
    {
        throw new NotSupportedException("This import method requires that the Payroll is saved. There would be an alternate flow to add calculations as part of new Payroll, but that is only for new Payrolls.");
    }
    var payrollApi = new Salaxy.PayrollClient(api.GetAuthClient()) { BaseUrl = api.Server };
    var personalIds = rows.Select(x => x.PersonalId).Distinct().ToArray();
    // Figures out which personal ID's do not have a calculation in the current Payroll.
    var personalIdsToAdd = personalIds.Where(id => !payroll.Calcs.Any(
        calc => isSamePersonalId(id, calc.Worker.PaymentData.SocialSecurityNumberValid) // SocialSecurityNumberValid is always either valid or null. SocialSecurityNumber field may be anything that was input.
        )).ToArray();
    if (personalIdsToAdd.Length > 0)
    {
        try
        {
            payroll = await payrollApi.AddEmploymentsAsync(payroll, string.Join(",", personalIdsToAdd));
        }
        catch (ApiException ex)
        {
            // This will throw if there is no employment relation with given Personal ID.
            api.ReportError(ex);
            return null;
        }
    }
    // [... continues - see below]
```
 
You could use the same code to add calculations based on employment relation IDs as well as personal IDs. Employment ID is the original parameters, Personal ID is a new shortcut.

The same code then continues to add also the rows to calculations and saves them:
```c#

    foreach (var rowGroup in rows.GroupBy(x => x.PersonalId))
    {
        string mySourceId = "SalaxyDemo.Import"; // Identify the rows that we are adding
        var calc = payroll.Calcs.First(x => isSamePersonalId(rowGroup.Key, x.Worker.PaymentData.SocialSecurityNumberValid));
        // Remove the rows that we have added in a previous run, but not the ones that were added manually in Palkkaus.
        // Depending on the usecase, you may choose to have some other logic here (e.g. always adding rows)
        calc.Rows = calc.Rows.Where(x => x.SourceId != mySourceId).ToList();
        foreach (var importRow in rowGroup)
        {
            calc.Rows.Add(MapRow(importRow, mySourceId));
        }
        calc = await payrollApi.CalculationSaveAsync(calc, payroll.Id);
    }
    // Refresh the Payroll from the storage
    payroll = await payrollApi.CrudGetAsync(payroll.Id);
    return payroll;
}
```
 
Important part is also the MapRow method, which shows some examples of our row types:
```c#
/// <summary>
/// Example of how to map random iport rows to Salaxy UserDefinedRow.
/// </summary>
/// <param name="row">Input row - an example data type.</param>
/// <param name="sourceId"></param>
/// <returns></returns>
public UserDefinedRow MapRow(SomeCustomImportRow row, string sourceId)
{
    var type = CalculationRowType.Unknown;
    dynamic data = null;
    switch (row.Type)
    {
        case SomeCustomImportRowType.Lauantai:
            type = CalculationRowType.SaturdayAddition;
            break;
        case SomeCustomImportRowType.Puhelinetu:
            type = CalculationRowType.PhoneBenefit;
            break;
        case SomeCustomImportRowType.Sunnuntai:
            type = CalculationRowType.SundayWork;
            break;
        case SomeCustomImportRowType.Tuntipalkka:
            type = CalculationRowType.HourlySalary;
            break;
        case SomeCustomImportRowType.Ylityo:
            type = CalculationRowType.Overtime;
            break;
        case SomeCustomImportRowType.Autoetu:
            type = CalculationRowType.CarBenefit;
            // You would need to get this information from somewhere else - outside the scope of this demo.
            data = new CarBenefitUsecase() {
                AgeGroup = AgeGroupCode.A,
                Deduction = 300,
                Kind = CarBenefitKind.FullCarBenefit
            };
            break;
        case SomeCustomImportRowType.Polkupyoraetu:
            // This is an example of how to define something special that is not available in our Row types
            // => You can just define the behavior to Incomes registry.
            type = CalculationRowType.IrIncomeType;
            data = new IrIncomeTypeUsecase()
            {
                Kind = TransactionCode.BicycleBenefitTaxable,
                // NOTE: This does not really make any sense: Just an example for defining very fine tuned Incomes Register stuff.
                IrData = new IrDetails() {
                    Flags = new[] {
                        IrFlags.OneOff,
                    },
                    InsuranceExceptions = new[] {
                        IrInsuranceExceptions.IncludeAccidentInsurance,
                    },
                },
            };
            break;
        case SomeCustomImportRowType.Undefined:
        default:
            throw new NotSupportedException($"Row type {row.Type} not supported.");
    }
    // NOTES:
    // Unit is typically set based on the row type, so you do not need to set it except in very special cases.
    // Message is set based on row types - language versioned. But of course you may want to set it to something coming from your system.
    // RowIndex comes from server (really only for legacy use) you cannot set it.
    return new UserDefinedRow() {
        Count = row.HoursOrCount,
        Price = row.PricePerHour,
        RowType = type,
        SourceId = sourceId,
        Data = data,
    };
}
```
 

