﻿using Salaxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaxyDemo.Model.ImportDemo
{
    /// <summary>
    /// Demonstrates the import functionalities to Palkkaus.fi / Salaxy
    /// </summary>
    public class SomeCustomImportService
    {
        /// <summary>
        /// Gets some demo rows as starting poist of demo
        /// </summary>
        /// <returns>Some demo data</returns>
        public List<SomeCustomImportRow> GetDemoRows()
        {
            var result = new List<SomeCustomImportRow>();
            // Note about personal ID's: Personal ID's are random - matches to real ID's are unintended.
            // Salaxy modifies "*" to a valid checksum (only technically valid ID's are accepted). 
            result.Add(new SomeCustomImportRow("200979-325*", SomeCustomImportRowType.Tuntipalkka, 120, 16.80, Salaxy.CalculationRowType.CarBenefit));
            result.Add(new SomeCustomImportRow("200979-325*", SomeCustomImportRowType.Sunnuntai, 8, 16.80, Salaxy.CalculationRowType.Salary));
            result.Add(new SomeCustomImportRow("150292-468*", SomeCustomImportRowType.Tuntipalkka, 20, 14.10, Salaxy.CalculationRowType.HourlySalary));
            return result;
        }

        /// <summary>
        /// Does a demo import to an existing Payroll object
        /// </summary>
        /// <param name="payroll">Payroll to update</param>
        /// <param name="rows">Rows to import</param>
        /// <param name="api">Authenticated Salaxy API helper instance</param>
        /// <returns>Stored Payroll if the import was successfull, null if it was not.</returns>
        public async Task<PayrollDetails> ImportRows(PayrollDetails payroll, List<SomeCustomImportRow> rows, SalaxyApiHelper api)
        {
            if (payroll.Id == null)
            {
                throw new NotSupportedException("This import method requires that the Payroll is saved. There would be an alternate flow to add calculations as part of new Payroll, but that is only for new Payrolls.");
            }
            var payrollApi = new Salaxy.PayrollClient(api.GetAuthClient()) { BaseUrl = api.Server };
            var personalIds = rows.Select(x => x.PersonalId).Distinct().ToArray();
            // Figures out which personal ID's do not have a calculation in the current Payroll.
            var personalIdsToAdd = personalIds.Where(id => !payroll.Calcs.Any(
                calc => isSamePersonalId(id, calc.Worker.PaymentData.SocialSecurityNumberValid) // SocialSecurityNumberValid is always either valid or null. SocialSecurityNumber field may be anything that was input.
                )).ToArray();
            if (personalIdsToAdd.Length > 0)
            {
                try
                {
                    payroll = await payrollApi.AddEmploymentsAsync(payroll, string.Join(",", personalIdsToAdd));
                }
                catch (ApiException ex)
                {
                    // This will throw if there is no employment relation with given Personal ID.
                    api.ReportError(ex);
                    return null;
                }
            }
            foreach (var rowGroup in rows.GroupBy(x => x.PersonalId))
            {
                string mySourceId = "SalaxyDemo.Import"; // Identify the rows that we are adding
                var calc = payroll.Calcs.First(x => isSamePersonalId(rowGroup.Key, x.Worker.PaymentData.SocialSecurityNumberValid));
                // Remove the rows that we have added in a previous run, but not the ones that were added manually in Palkkaus.
                // Depending on the usecase, you may choose to have some other logic here (e.g. always adding rows)
                calc.Rows = calc.Rows.Where(x => x.SourceId != mySourceId).ToList();
                foreach (var importRow in rowGroup)
                {
                    calc.Rows.Add(MapRow(importRow, mySourceId));
                }
                calc = await payrollApi.CalculationSaveAsync(calc, payroll.Id);
            }
            // Refresh the Payroll from the storage
            payroll = await payrollApi.CrudGetAsync(payroll.Id);
            return payroll;
        }

        /// <summary>
        /// Example of how to map random iport rows to Salaxy UserDefinedRow.
        /// </summary>
        /// <param name="row">Input row - an example data type.</param>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public UserDefinedRow MapRow(SomeCustomImportRow row, string sourceId)
        {
            var type = CalculationRowType.Unknown;
            dynamic data = null;
            switch (row.Type)
            {
                case SomeCustomImportRowType.Lauantai:
                    type = CalculationRowType.SaturdayAddition;
                    break;
                case SomeCustomImportRowType.Puhelinetu:
                    type = CalculationRowType.PhoneBenefit;
                    break;
                case SomeCustomImportRowType.Sunnuntai:
                    type = CalculationRowType.SundayWork;
                    break;
                case SomeCustomImportRowType.Tuntipalkka:
                    type = CalculationRowType.HourlySalary;
                    break;
                case SomeCustomImportRowType.Ylityo:
                    type = CalculationRowType.Overtime;
                    break;
                case SomeCustomImportRowType.Autoetu:
                    type = CalculationRowType.CarBenefit;
                    // You would need to get this information from somewhere else - outside the scope of this demo.
                    data = new CarBenefitUsecase() {
                        AgeGroup = AgeGroupCode.A,
                        Deduction = 300,
                        Kind = CarBenefitKind.FullCarBenefit
                    };
                    break;
                case SomeCustomImportRowType.Polkupyoraetu:
                    // This is an example of how to define something special that is not available in our Row types
                    // => You can just define the behavior to Incomes registry.
                    type = CalculationRowType.IrIncomeType;
                    data = new IrIncomeTypeUsecase()
                    {
                        Kind = TransactionCode.BicycleBenefitTaxable,
                        // NOTE: This does not really make any sense: Just an example for defining very fine tuned Incomes Register stuff.
                        IrData = new IrDetails() {
                            Flags = new[] {
                                IrFlags.OneOff,
                            },
                            InsuranceExceptions = new[] {
                                IrInsuranceExceptions.IncludeAccidentInsurance,
                            },
                        },
                    };
                    break;
                case SomeCustomImportRowType.Undefined:
                default:
                    throw new NotSupportedException($"Row type {row.Type} not supported.");
            }
            // NOTES:
            // Unit is typically set based on the row type, so you do not need to set it except in very special cases.
            // Message is set based on row types - language versioned. But of course you may want to set it to something coming from your system.
            // RowIndex comes from server (really only for legacy use) you cannot set it.
            return new UserDefinedRow() {
                Count = row.HoursOrCount,
                Price = row.PricePerHour,
                RowType = type,
                SourceId = sourceId,
                Data = data,
            };
        }

        /// <summary>
        /// Taking into account that the last character may be asterisk
        /// (in test only and in input only: in saved data the asterisk is calculated to check sum).
        /// Also assures uppercase - in saved data, characters are always upper case.
        /// Also make sure that null value is not matched.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        private bool isSamePersonalId(string first, string second)
        {
            if (first == null || second == null)
            {
                return false;
            }
            return first.Trim().ToUpper().Substring(0, 10) == second.Trim().ToUpper().Substring(0, 10);
        }
    }
}
