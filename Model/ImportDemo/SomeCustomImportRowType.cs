﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaxyDemo.Model.ImportDemo
{
    /// <summary>
    /// Defines the type of single import in a row
    /// </summary>
    public enum SomeCustomImportRowType
    {
        Undefined = 0,

        Tuntipalkka = 1,

        // Palkan lisät
        Sunnuntai = 100,

        Lauantai = 101,

        Ylityo = 110,

        // Luontoisedut
        Puhelinetu = 300,

        Autoetu = 310,

        Polkupyoraetu,

    }
}
