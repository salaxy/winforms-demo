﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaxyDemo.Model.ImportDemo
{
    /// <summary>
    /// This is an example of some custom import type from some external system.
    /// </summary>
    public class SomeCustomImportRow
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public SomeCustomImportRow()
        {}

        /// <summary>
        /// Default constructor creates the item with most common properties.
        /// </summary>
        /// <param name="id">Personal ID for the Worker</param>
        /// <param name="type">Type of the row</param>
        /// <param name="count">Count: typically number of hours, but sometimes also days etc.</param>
        /// <param name="price">Price of an hour, day month etc.</param>
        public SomeCustomImportRow(string id, SomeCustomImportRowType type, double count, double price, Salaxy.CalculationRowType sxyType)
        {
            PersonalId = id;
            Type = type;
            HoursOrCount = count;
            PricePerHour = price;
            SalaxyType = sxyType;
        }

        /// <summary>
        /// Personal ID for the Worker
        /// </summary>
        public string PersonalId { get; set; }

        /// <summary>
        /// Count: typically number of hours, but sometimes also days etc.
        /// </summary>
        public double HoursOrCount { get; set; }

        /// <summary>
        /// Price of an hour, day month etc.
        /// </summary>
        public double PricePerHour { get; set; }

        /// <summary>
        /// Type of the row
        /// </summary>
        public SomeCustomImportRowType Type { get; set; }

        /// <summary>
        /// Temp
        /// </summary>
        public Salaxy.CalculationRowType SalaxyType { get; set; }

    }
}
