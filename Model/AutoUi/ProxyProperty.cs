﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaxyDemo.Model.AutoUi
{
    /// <summary>
    /// Added custom support for Property Grid without changing the classes code generation.
    /// Basic idea is here: https://stackoverflow.com/questions/60515438/how-to-use-propertygrid-to-allow-editing-properties-without-a-setter
    /// </summary>
    public class ProxyProperty
    {
        public ProxyProperty(string name, object value)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(value));

            Name = name;
            Value = value;
            Attributes = new List<Attribute>();
        }

        public ProxyProperty(object instance, PropertyDescriptor descriptor)
        {
            if (descriptor == null)
            {
                throw new ArgumentNullException(nameof(descriptor));
            }
            var type = descriptor.PropertyType;

            Name = descriptor.Name;
            
            var def = descriptor.Attributes.OfType<DefaultValueAttribute>().FirstOrDefault();
            if (def != null)
            {
                HasDefaultValue = true;
                DefaultValue = def.Value;
            }

            IsReadOnly = (descriptor.Attributes.OfType<ReadOnlyAttribute>().FirstOrDefault()?.IsReadOnly).GetValueOrDefault();
            ShouldSerializeValue = descriptor.ShouldSerializeValue(instance);
            Attributes = descriptor.Attributes.Cast<Attribute>().ToList();
            if (!type.IsValueType && !type.FullName.StartsWith("System."))
            {
                Value = new Proxy(descriptor.GetValue(instance));
                Attributes.Add(new TypeConverterAttribute(typeof(ExpandableObjectConverter)));
                IsReadOnly = true; // Override for those that can be edited.
            }
            else
            {
                Value = descriptor.GetValue(instance);
            }
            PropertyType = descriptor.PropertyType;
        }

        public string Name { get; }
        public object Value { get; set; }
        public object DefaultValue { get; set; }
        public bool HasDefaultValue { get; set; }
        public bool IsReadOnly { get; set; }
        public bool ShouldSerializeValue { get; set; }
        public Type PropertyType { get; set; }
        public IList<Attribute> Attributes { get; }

        public override string ToString()
        {
            return "Property: " + Name;
        }
    }
}
