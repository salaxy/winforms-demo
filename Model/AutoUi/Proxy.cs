﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaxyDemo.Model.AutoUi
{
    /// <summary>
    /// Added custom support for Property Grid without changing the classes code generation.
    /// Basic idea is here: https://stackoverflow.com/questions/60515438/how-to-use-propertygrid-to-allow-editing-properties-without-a-setter
    /// </summary>
    public class Proxy : ICustomTypeDescriptor
    {
        public Proxy(object instance)
        {
            if (instance == null)
                throw new ArgumentNullException(nameof(instance));

            Instance = instance;
            Properties = TypeDescriptor.GetProperties(instance).OfType<PropertyDescriptor>().Select(d => new ProxyProperty(instance, d)).ToDictionary(p => p.Name);
        }

        public object Instance { get; }
        public IDictionary<string, ProxyProperty> Properties { get; }

        public AttributeCollection GetAttributes() => TypeDescriptor.GetAttributes(Instance);
        public string GetClassName() => TypeDescriptor.GetClassName(Instance);
        public string GetComponentName() => TypeDescriptor.GetComponentName(Instance);
        public TypeConverter GetConverter() => TypeDescriptor.GetConverter(Instance);
        public EventDescriptor GetDefaultEvent() => TypeDescriptor.GetDefaultEvent(Instance);
        public object GetEditor(Type editorBaseType) => TypeDescriptor.GetEditor(Instance, editorBaseType);
        public EventDescriptorCollection GetEvents() => TypeDescriptor.GetEvents(Instance);
        public EventDescriptorCollection GetEvents(Attribute[] attributes) => TypeDescriptor.GetEvents(Instance, attributes);
        public PropertyDescriptor GetDefaultProperty() => TypeDescriptor.GetDefaultProperty(Instance);
        public PropertyDescriptorCollection GetProperties() => new PropertyDescriptorCollection(Properties.Values.Select(p => new Desc(this, p)).ToArray());
        public PropertyDescriptorCollection GetProperties(Attribute[] attributes) => GetProperties();
        public object GetPropertyOwner(PropertyDescriptor pd) => Instance;

        public override string ToString()
        {
            return "...";
        }

        private class Desc : PropertyDescriptor
        {
            public Desc(Proxy proxy, ProxyProperty property)
                : base(property.Name, property.Attributes.ToArray())
            {
                Proxy = proxy;
                Property = property;
            }

            public Proxy Proxy { get; }
            public ProxyProperty Property { get; }

            public override Type ComponentType => Proxy.GetType();
            public override Type PropertyType => Property.PropertyType ?? typeof(object);
            public override bool IsReadOnly => Property.IsReadOnly;
            public override bool CanResetValue(object component) => Property.HasDefaultValue;
            public override object GetValue(object component) => Property.Value;
            public override void ResetValue(object component) { if (Property.HasDefaultValue) Property.Value = Property.DefaultValue; }
            public override void SetValue(object component, object value) => Property.Value = value;
            public override bool ShouldSerializeValue(object component) => Property.ShouldSerializeValue;
        }

    }
}
