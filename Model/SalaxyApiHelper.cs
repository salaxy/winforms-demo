﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Salaxy;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SalaxyDemo.Model
{
    /// <summary>
    /// Provides helper methods for connecting to Salaxy API.
    /// </summary>
    public class SalaxyApiHelper
    {
        /// <summary>
        /// If authenticated, this is the token.
        /// </summary>
        public string Token { get; private set; }

        /// <summary>
        /// The API server, default is "https://demo-secure.salaxy.com".
        /// </summary>
        public string Server { get; private set; } = "https://demo-secure.salaxy.com";

        /// <summary>
        /// If authenticated, this is the User name (e-mail).
        /// </summary>
        public string Uid { get; private set; }

        /// <summary>
        /// Returns true if the user has been authenticated.
        /// </summary>
        public bool IsAuthenticated
        {
            get { return Session != null; }
        }

        /// <summary>
        /// The user session is user is authenticated
        /// </summary>
        public Salaxy.UserSession Session { get; private set; }

        /// <summary>
        /// Gets a new authenticated client. The token must be set before calling this method or an exception will ber thrown.
        /// </summary>
        /// <returns>Http client with authentication.</returns>
        public HttpClient GetAuthClient()
        {
            if (!IsAuthenticated)
            {
                throw new Exception("No token: There is no token set in the HttpHelpers. Please login first.");
            }
            var result = new HttpClient();
            result.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            return result;
        }

        /// <summary>
        /// Gets an anonymous client.
        /// </summary>
        /// <returns>Http client without authentication (anonymous).</returns>
        public HttpClient GetAnonClient()
        {
            return new HttpClient();
        }

        /// <summary>
        /// Does Password Authorization (RFC6749 4.3. "Resource Owner Password Credentials Grant"), sets the token and fetches User session info.
        /// </summary>
        /// <param name="username">Username used in authentication.</param>
        /// <param name="password">Password used in authentication.</param>
        /// <param name="server">Server address for Oauth. "https://demo-secure.salaxy.com" is default. Other options are "https://test-secure.salaxy.com" and "https://secure.salaxy.com".</param>
        public async Task DoPasswordAuthorization(string username, string password, string server = "https://demo-secure.salaxy.com")
        {
            var request = new { grant_type = "password", username, password };
            var json = JsonConvert.SerializeObject(request);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var client = GetAnonClient();
            var response = await client.PostAsync($"{server}/oauth2/token", data);
            var resultString = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Authentication failed:" + resultString);
            }
            dynamic result = JsonConvert.DeserializeObject<ExpandoObject>(resultString, new ExpandoObjectConverter());
            if (string.IsNullOrEmpty(result.access_token))
            {
                throw new Exception("access_token is null/empty");
            }
            Token = result.access_token;
            Server = server;
            Uid = username;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            Session = await new Salaxy.SessionClient(client).GetSessionAsync();
        }

        /// <summary>
        /// Performs a search on the grid list.
        /// TODO: This sample does not handle paging / load more logic.
        /// </summary>
        /// <param name="search">Search string</param>
        /// <param name="filter">OData filter query, e.g. "shortText eq 'foobar'".</param>
        /// <param name="orderby">OData order by query, e.g. "shortText DESC".</param>
        /// <returns>Gets the items as an array.</returns>
        public async Task<List<dynamic>> Search(ApiListItemType type, string search = null, string filter = null, string orderby = null)
        {
            switch (type)
            {
                case ApiListItemType.Calculation:
                    {
                        var api = new CalcEditableClient(GetAuthClient()) { BaseUrl = Server };
                        var result = await api.ODataSearchEditableAsync(search, null, filter, orderby, null, null, false);
                        return result.Items.Cast<dynamic>().ToList();
                    }
                case ApiListItemType.CalculationPaid:
                    {
                        var api = new CalcReadonlyClient(GetAuthClient()) { BaseUrl = Server };
                        var result = await api.ODataSearchReadonlyAsync(search, null, filter, orderby, null, null, false);
                        return result.Items.Cast<dynamic>().ToList();
                    }
                case ApiListItemType.WorkerAccount:
                    {
                        var api = new WorkersClient(GetAuthClient()) { BaseUrl = Server };
                        var result = await api.ODataSearchAsync(search, null, filter, orderby, null, null, false);
                        return result.Items.Cast<dynamic>().ToList();
                    }
                case ApiListItemType.Employment:
                    {
                        var api = new EmploymentClient(GetAuthClient()) { BaseUrl = Server };
                        var result = await api.ODataSearchAsync(search, null, filter, orderby, null, null, false);
                        return result.Items.Cast<dynamic>().ToList();
                    }
                case ApiListItemType.PayrollDetails:
                    {
                        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
                        var result = await api.ODataSearchAsync(search, null, filter, orderby, null, null, false);
                        return result.Items.Cast<dynamic>().ToList();
                    }
                default:
                    throw new NotImplementedException($"list item type {type} is not supported in this demo.");
            }
        }

        /// <summary>
        /// Gets a single item from the repository
        /// </summary>
        /// <param name="type">Type of item to fetch.</param>
        /// <param name="id">Identifier of the item</param>
        /// <returns>The item from the API.</returns>
        public async Task<dynamic> GetItem(ApiListItemType type, string id)
        {
            // TODO: Make strongly typed overload based on generic types.
            switch (type)
            {
                case ApiListItemType.Calculation:
                    {
                        var api = new CalcEditableClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudGetAsync(id);
                    }
                case ApiListItemType.CalculationPaid:
                    {
                        var api = new CalcReadonlyClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudGetAsync(id);
                    }
                case ApiListItemType.WorkerAccount:
                case ApiListItemType.Employment:
                    {
                        var api = new WorkersClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudGetAsync(id);
                    }
                case ApiListItemType.PayrollDetails:
                    {
                        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudGetAsync(id);
                    }
                default:
                    throw new NotImplementedException($"list item type {type} is not supported in this demo.");
            }
        }

        /// <summary>
        /// Save an item into the storage.
        /// </summary>
        /// <param name="type">Type of item</param>
        /// <param name="item">Item to save</param>
        /// <returns>Item after saving it to the database.</returns>
        public async Task<dynamic> SaveItem(ApiListItemType type, dynamic item)
        {
            // TODO: Make strongly typed overload based on generic types.
            switch (type)
            {
                case ApiListItemType.Calculation:
                    {
                        var api = new CalcEditableClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudSaveAsync(item);
                    }
                case ApiListItemType.CalculationPaid:
                    {
                        var api = new CalcReadonlyClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.SaveCalculationAsync(item);
                    }
                case ApiListItemType.WorkerAccount:
                    {
                        var api = new WorkersClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudSaveAsync(item);
                    }
                case ApiListItemType.Employment:
                    throw new NotImplementedException("Not implementd in this demo. Ask for more info.");
                case ApiListItemType.PayrollDetails:
                    {
                        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudSaveAsync(item);
                    }
                default:
                    throw new NotImplementedException($"list item type {type} is not supported in this demo.");
            }
        }

        /// <summary>
        /// Creates a new item on server-side.
        /// </summary>
        /// <param name="type">Type of item to create</param>
        /// <returns>A new item with default objects set.</returns>
        public async Task<dynamic> CreateNew(ApiListItemType type)
        {
            // TODO: This could safely be done on client-side without async round-tripping to server:
            //       Just need to initialize objects and arrays with empty objects and arrays (like in @salaxy/core helpers).
            switch (type)
            {
                case ApiListItemType.Calculation:
                    {
                        var api = new CalcEditableClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudGetNewAsync();
                    }
                case ApiListItemType.CalculationPaid:
                    throw new NotSupportedException("Cannot create a paid calculation - create Draft first and then mark it paid.");
                case ApiListItemType.WorkerAccount:
                case ApiListItemType.Employment:
                    throw new NotImplementedException("Not implementd in this demo. Ask for more info.");
                case ApiListItemType.PayrollDetails:
                    {
                        var api = new PayrollClient(GetAuthClient()) { BaseUrl = Server };
                        return await api.CrudGetNewAsync();
                    }
                default:
                    throw new NotImplementedException($"list item type {type} is not supported in this demo.");
            }
        }

        /// <summary>
        /// Recalculates a calculation.
        /// </summary>
        /// <param name="calc">Calculation to recalculate</param>
        /// <returns>Recalculated calculation</returns>
        public async Task<Calculation> Recalculate(Calculation calc)
        {
            return await (new CalculatorClient(IsAuthenticated ? GetAuthClient() : GetAnonClient()) { BaseUrl = Server }).RecalculateAsync(calc);
        }

        /// <summary>
        /// Clears the authentication data.
        /// </summary>
        public void LogOut()
        {
            Token = null;
            Server = "https://demo-secure.salaxy.com";
            Uid = null;
            Session = null;
        }

        /// <summary>
        /// Reports an exception from the API to the end user and continues.
        /// </summary>
        /// <param name="apiException"></param>
        public void ReportError(ApiException apiException)
        {
            if (string.IsNullOrEmpty(apiException.Response))
            {
                System.Windows.Forms.MessageBox.Show($"Unknown error with status code {apiException.StatusCode}.");
                return;
            }
            try
            {
                var result = JsonConvert.DeserializeObject<Dictionary<string, object>>(apiException.Response);
                var error = result.ContainsKey("error") ? result["error"].ToString() : null;
                if (result.ContainsKey("messageHtml"))
                {
                    System.Windows.Forms.MessageBox.Show(result["messageHtml"].ToString(), error ?? "Error");
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(error ?? $"Unknown error with status code {apiException.StatusCode} and response: {apiException.Response}");
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"Cannot parse response of status code {apiException.StatusCode}: {apiException.Response}");
                return;
            }
        }
    }
}
